import urwid

from modules import (
    screen,
    ui,
)

class MenuScreen(screen.BaseScreen):
    def init_ui(self):
        self.widgets.menu = urwid.ListBox()
        actions = []
        self.add_actions(actions)
        self.action_tracker.add_actions(self.widgets.menu, actions)

        self.widgets.help = urwid.Text('')
        self.widgets.help_frame = urwid.LineBox(self.widgets.help)

        title = self.get_title()
        if title:
            self.app.set_header(('header', ' {} '.format(title)))
        self.widgets.pile = urwid.Pile([self.widgets.menu])
        return self.widgets.pile

    def load_data(self):
        self.keys = self.load_keys()
        walker = ui.PrettyListWalker((self.format_item(key) for key in self.keys),
                                     self.app.focus_map, self.app.passive_map)
        self.widgets.menu.body = walker
        urwid.connect_signal(walker, 'focus-changed', self.show_help)
        self.show_help()

    @property
    def current_key(self):
        """Returns the currently focused key. Can be None."""
        try:
            return self.keys[self.widgets.menu.focus_position]
        except IndexError:
            # empty listbox
            return None

    def update_current_item(self):
        index = self.widgets.menu.focus_position
        text = self.format_item(self.keys[index])
        self.widgets.menu.body[index].original_widget.set_text(text)

    def delete_current_item(self):
        index = self.widgets.menu.focus_position
        del self.widgets.menu.body[index]
        del self.keys[index]

    def add_key(self, key):
        self.keys.append(key)
        self.widgets.menu.body.append(self.format_item(key))

    def show_help(self):
        text = None
        key = self.current_key
        if key is not None:
            text = self.get_help(key)
        if text:
            self.widgets.help.set_text(text)
            if len(self.widgets.pile.contents) == 1:
                self.widgets.pile.contents.append((self.widgets.help_frame, ('pack', None)))
        else:
            if len(self.widgets.pile.contents) == 2:
                del self.widgets.pile.contents[1]

    def add_actions(self, actions):
        return

    def get_title(self):
        return None

    def load_keys(self):
        return ()

    def format_item(self, key):
        return key

    def get_help(self, key):
        return None
