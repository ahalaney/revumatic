import functools
import itertools
import os
import re
import subprocess
import urllib.parse
import urwid
import weakref

from modules.settings import settings
from modules.utils import lab
from modules import (
    formatters,
    git,
    gitlab,
    mr_activity,
    mr_ci,
    mr_commits,
    mr_layout,
    mr_ui,
    patch,
    screen,
    storage,
    ui,
    utils,
)


class LayoutError(Exception):
    pass


class Screen(screen.BaseScreen):
    help = 'open a merge request'
    ui_arg = { 'name': 'mr_id', 'type': str, 'display_name': 'Merge request URL' }

    @classmethod
    def register_settings(cls):
        sect = settings.section('mr', 'Merge Request view options')
        sect.register('swap-compare', bool, default=False,
                      display_name='Swap sides in commit comparison',
                      help_text='''By default, the backported commit is on the left side,
                                   while upstream commit is on the right side. Similarly,
                                   when comparing MR versions, the current commit is on the
                                   left side, while the previous commit is on the right side.
                                   Setting this to 'yes' will swap the sides.''')
        sect.register('keep-todo', bool,
                      display_name='Keep MR in TODO list',
                      help_text='''When commenting on a merge request that is in the TODO
                                   list, GitLab automatically removes it from the TODO list.
                                   With this config option set to 'yes', Revumatic will
                                   add such merge request back to the TODO list. If unset,
                                   Revumatic will ask you every time.''')
        all_layouts = (utils.from_camel_case(a[:-6]) for a in dir(mr_layout)
                                                     if a.endswith('Layout'))
        sect.register('layout', tuple(all_layouts), default='standard',
                      display_name='UI layout',
                      help_text='''The user interface layout to use. The standard layout is
                                   suitable for small to mid-size screens. If you have
                                   a large terminal, you may find one of the other layouts
                                   to be more practical.''')

    def __init__(self, app, args):
        super().__init__(app, args)
        mr_id = args.mr_id
        self.project_path = None
        if mr_id.startswith('http'):
            mr_id = mr_id.rstrip('/')
            url = urllib.parse.urlsplit(mr_id)
            if (url.scheme not in ('http', 'https') or url.query):
                raise screen.ScreenError('Invalid merge request URL: {}'.format(mr_id))
            if url.netloc != settings['global']['gitlab']:
                raise screen.ScreenError('The server in the URL does not match the server in the config.')
            path = url.path
            if path.startswith('/'):
                path = path[1:]
            components = path.rsplit('/', maxsplit=3)
            if (len(components) != 4 or components[1] != '-' or
                    components[2] != 'merge_requests'):
                raise screen.ScreenError('Invalid merge request URL: {}'.format(mr_id))
            self.project_path = components[0]
            mr_id = components[3]
        try:
            self.mr_id = int(mr_id)
        except ValueError:
            raise screen.ScreenError('The merge request parameter should be a number or an URL.')
        self.show_commit_comments = True
        self.lock = None

    def load_data(self):
        # build self.repos and resolve project_path if we didn't get the
        # full MR URL
        paths = settings['global']['repos']
        self.repos = git.RepoCollection(paths, settings['global']['gitlab'])
        if not self.project_path:
            try:
                self.project_path = self.repos.add_cur_dir()
            except git.GitError as e:
                self.app.message_popup(self.quit, str(e))
                return
        # take the MR lock
        lock_name = '{}!{}'.format(self.project_path.replace('/', '-'), self.mr_id)
        self.lock = settings.dir.try_lock(lock_name)
        if not self.lock:
            self.app.message_popup(self.quit, 'This MR is already opened in another Revumatic instance.')
            return
        # find the right repo
        self.check_repo()

    def check_repo(self, **kwargs):
        try:
            self.repo, self.pkg = self.repos.find_repo(self.project_path)
        except git.GitConfigError as e:
            self.repo_retry(e, self.check_repo, start=True)
            return
        except git.GitBadProject as e:
            self.app.message_popup(self.quit, str(e))
            return
        if not self.repo:
            self.app.edit_popup(self.add_repo,
                'Enter the local file system path to a repository for {}:'.format(self.project_path),
                allow_esc=True)
            return

        self.project = lab.project(self.repo.project)
        self.load_mr()

    def add_repo(self, text=None):
        path = text
        if path is None:
            self.quit()
            return
        assert self.project_path
        try:
            self.repos.add_repo(path, self.project_path)
        except git.GitConfigError as e:
            cb = functools.partial(self.add_repo, path)
            self.repo_retry(e, cb, start=True)
            return
        except git.GitError as e:
            self.app.message_popup(self.check_repo, str(e))
            return
        settings['global'].add('repos', path)
        settings.save()
        self.check_repo()

    def repo_retry(self, exc, callback, button=None, start=False):
        if start:
            self.app.buttons_popup(self.repo_retry, str(exc),
                                   ('~dismiss', '~fix it for me'),
                                   allow_esc=True,
                                   callback_args=(exc, callback))
            return
        if button and button.startswith('fix'):
            subprocess.run(exc.suggested_command, check=True)
            self.app.redraw_screen()
            callback()
            return
        self.quit()

    def init_ui(self):
        self.widgets.commit_list = urwid.ListBox()
        comp1 = self.widgets.content_backport = urwid.Viewer('', wrap='clip')
        comp2 = self.widgets.content_upstream = urwid.Viewer('', wrap='clip')
        if settings['mr']['swap-compare']:
            comp1, comp2 = comp2, comp1
        self.widgets.content_compare = mr_ui.Coupler(comp1, comp2)

        layout_cls_name = utils.to_camel_case(settings['mr']['layout']) + 'Layout'
        layout_class = getattr(mr_layout, layout_cls_name)
        self.layout = layout_class(self.app.workplace,
                                   self.widgets.commit_list,
                                   urwid.TextBox(self.widgets.content_compare))
        self.content_defs = self.layout.get_content_defs()
        self.widgets.content = [ui.JailedPlaceholder(urwid.SolidFill())
                                for _ in self.content_defs]

        interdiff_hlp = 'show the difference between the commit and upstream or between the commit versions'
        common_actions = [
            { 'name': 'interdiff', 'prio': 3, 'help': 'interdiff',
              'long_help': interdiff_hlp },
            { 'name': 'full-activity', 'prio': 2, 'help': 'all activity',
              'long_help': 'show/hide full merge request activity' },
            { 'name': 'next-item', 'prio': 1, 'help': 'next', 'long_help': 'next commit' },
            { 'name': 'prev-item', 'prio': 1, 'long_help': 'previous commit' },
        ]
        commit_list_actions = [
            { 'name': 'bugzilla', 'prio': 4, 'help': 'open Bugzilla' },
            { 'name': 'gitlab', 'prio': 4, 'help': 'open in GitLab' },
            { 'name': 'compare-versions', 'prio': 4, 'help': 'compare versions' },
            { 'name': 'compare-last-approved-version', 'prio': 4,
              'long_help': 'compare with the last approved version' },
            { 'name': 'submit-comments', 'prio': 4, 'help': 'submit comments' },
            { 'name': 'approve', 'prio': 4, 'help': 'approve',
              'confirm': 'Do you want to approve the merge request?' },
            { 'name': 'unapprove', 'prio': 4, 'help': 'unapprove',
              'confirm': 'Do you want to unapprove the merge request?' },
            { 'name': 'assign', 'prio': 4, 'help': 'assign to me',
              'confirm': 'Do you want to assign the merge request to yourself?' },
            { 'name': 'unassign', 'prio': 4, 'help': 'unassign me',
              'confirm': 'Do you want to unassign yourself from the merge request?' },
            { 'name': 'todo-add', 'prio': 4, 'help': 'todo',
              'long_help': 'add this merge request to the TODO list',
              'confirm': 'Do you want to add this merge request to your TODO list?' },
            { 'name': 'ci-status', 'prio': 4, 'help': 'CI status',
              'long_help': 'show CI status of the merge request' },
            { 'name': 'find-file-next', 'prio': 4, 'help': 'find file',
               'long_help': 'find the next commit touching a file',
               'enabled': False },
            { 'name': 'find-file-prev', 'prio': 4,
               'long_help': 'find the previous commit touching a file',
               'enabled': False },
            { 'name': 'to-content', 'prio': 4 },
            { 'name': 'quit', 'prio': 0, 'help': self.app.get_quit_text(),
              'confirm': 'Do you want to quit this merge request?' },
        ] + common_actions
        content_actions = [
            { 'name': 'to-commits', 'prio': 10, 'help': 'return',
              'long_help': 'return to the commit list' },
            { 'name': 'comment', 'prio': 4, 'help': 'comment',
              'long_help': 'add/edit comment at the cursor' },
            { 'name': 'new-thread', 'prio': 4, 'long_help': 'start a new thread' },
            { 'name': 'hide-comments', 'prio': 4, 'help': 'hide comments',
               'long_help': 'hide/show comments in the code' },
            { 'name': 'fullscreen', 'prio': 2, 'help': 'fullscreen',
              'long_help': 'switch fullscreen',
              'enabled': self.layout.fullscreen_supported },
            { 'name': 'search', 'prio': 2, 'help': 'search' },
            { 'name': 'search-next', 'prio': 2, 'help': 'search next' },
        ] + common_actions
        if self.layout.content_enter_supported:
            content_actions.append({ 'name': 'from-content', 'prio': 4 })
        content_diff_actions = content_actions + [
            { 'name': 'diff-next', 'prio': 2, 'help': 'next difference',
              'long_help': 'jump to the next difference' },
            { 'name': 'diff-prev', 'prio': 2,
              'long_help': 'jump to the previous difference' },
        ]

        self.action_tracker.add_actions(self.widgets.commit_list, commit_list_actions)
        self.action_tracker.add_actions(self.widgets.content_backport, content_diff_actions)
        self.action_tracker.add_actions(self.widgets.content_upstream, content_diff_actions)
        for w in self.widgets.content:
            self.action_tracker.add_actions(w, content_actions)

        urwid.connect_signal(self.widgets.content_backport, 'moved',
                             self.save_compare_position)
        urwid.connect_signal(self.widgets.content_upstream, 'moved',
                             self.save_compare_position)

        main_widget = self.layout.setup(*self.widgets.content)

        self.title_mode = None
        self.comments_to_save = []
        self.comments_drafts = set()
        self.last_search = None
        self.last_search_input = ''

        self.app.add_timer('save_comments', 10, self.save_comments)
        return main_widget

    def adjust_ui_mr_loaded(self):
        """Adjusts the ui after the MR got loaded."""
        self.activity.show(False)
        if self.upstream_diff.file_list:
            p = self.app.listbox_custom_popup(self.upstream_diff.file_list)
            self.widgets.file_popup, self.widgets.files = p
            self.action_tracker.add_actions(self.widgets.files,
                                            [{ 'name': 'confirm-find-file' }])

        templates = self.pkg.get_template_names()
        self.template_keys = [t[0] for t in templates]
        if templates:
            p = self.app.listbox_custom_popup([t[1] for t in templates])
            self.widgets.template_popup, self.widgets.templates = p
            self.action_tracker.add_actions(self.widgets.templates,
                                            [{ 'name': 'confirm-template' }])

    def quit(self, **kwargs):
        self.app.del_timer('save_comments')
        self.save_comments()
        if self.lock:
            self.lock.unlock()
        self.app.quit()

    def fetch(self):
        popup = self.app.wait_popup('Fetching commits...')
        self.repo.fetch()
        popup.stop()
        self.app.redraw_screen()

    def find_version_by_head(self, head_sha):
        for v in self.versions:
            if v.head_sha == head_sha:
                return v
        return None

    def mr_parse_user_list(self, data, action_name, user_key=None):
        result = []
        me = False
        user_id = lab.me['id']
        for user in data:
            if user_key:
                user = user_key(user)
            if user['id'] == user_id:
                me = True
            result.append(user)
        if self.mr['author']['id'] == user_id:
            # can't self-approve or self-assign
            self.action_tracker.set_action_enabled(action_name, False)
            self.action_tracker.set_action_enabled('un' + action_name, False)
        else:
            self.action_tracker.set_action_enabled(action_name, not me)
            self.action_tracker.set_action_enabled('un' + action_name, me)
        return result

    def mr_format_add_user_list(self, annot, user_list, name):
        if not user_list:
            return
        my_id = lab.me['id']
        annot.add(('desc-header', name + ':'))
        for i, user in enumerate(user_list):
            data = formatters.format_name(user)
            if i < len(user_list) - 1:
                data.append(',')
            annot.add(data)
        annot.add_break()

    def mr_format_cover_annotation(self):
        my_id = lab.me['id']
        annot = formatters.AnnotationFormatter('desc')
        annot.add(('desc-version', 'v{}'.format(self.latest_version.number)))
        annot.add('from')
        annot.add(('desc-date', '{:%Y-%m-%d %H:%M}'.format(self.latest_version.created_at)))
        annot.add_break()
        annot.add(('desc-header', 'Author:'))
        annot.add(formatters.format_name(self.mr['author'],
                                         email=self.mr.author['public_email']))
        annot.add_break()

        data = self.mr_parse_user_list(self.mr['assignees'], 'assign')
        self.mr_format_add_user_list(annot, data, 'Assigned to')
        data = self.mr_parse_user_list(self.mr.approvals['approved_by'],
                                       action_name='approve',
                                       user_key=lambda u: u['user'])
        self.mr_format_add_user_list(annot, data, 'Approved by')

        if (self.info['approved_version'] and
            self.info['approved_version'] < self.latest_version.number):
            annot.add(('desc', 'You last approved'))
            annot.add(('desc-version', 'v{}'.format(self.info['approved_version'])))
            annot.add_break()
        annot.add_empty_line()
        if self.mr['state'] != 'opened':
            annot.add(('desc-label-err', self.mr['state']), label=True)
        if self.mr['work_in_progress']:
            annot.add(('desc-label-err', 'draft'), label=True)
        blocking_threads = self.activity.count_blocking_threads()
        if blocking_threads:
            plural = 's' if blocking_threads > 1 else ''
            annot.add(('desc-label-err', ' {} blocking thread{} '
                                         .format(blocking_threads, plural)))
        annot.add_break()
        for label in self.mr['labels']:
            cls = self.pkg.get_label_color(label)
            if cls is None:
                continue
            if not cls:
                cls = 'desc-label'
            else:
                cls = 'desc-label-' + cls
            annot.add((cls, label), label=True)
        return annot.format()

    def remove_diffstat(self, data):
        return re.sub(r'(\n [^ ].*)+\n \d* files? changed(, \d* insertions?\(\+\))?(, \d* deletions?\(-\))?',
                      '', data)

    def mr_parse_info(self):
        self.info = {}
        self.info['bugzilla'] = self.pkg.get_bugzilla(self.mr)
        if not self.info['bugzilla']:
            self.action_tracker.set_action_enabled('bugzilla', False)

        last_approval = self.activity.get_last_approval(lab.me['id'])
        self.info['approved_version'] = last_approval
        if not last_approval or last_approval >= self.latest_version.number:
            self.action_tracker.set_action_enabled('compare-last-approved-version', False)

    def mr_get_disp_commit_list(self, old_version=None):
        if old_version:
            cls = mr_commits.DispCommitVersionList
            version = (old_version, self.latest_version)
        else:
            cls = mr_commits.DispCommitList
            version = self.latest_version
        annot = urwid.Viewer(self.mr_format_cover_annotation())
        annot.comment_handler = self.activity.general_comment_handler
        return cls(version, self.repo, self.repos, self.app, self.mr,
                   annot, self.activity.widget, self.content_defs)

    def format_mark(self, mark, cls_base='list'):
        cls = cls_base
        if mark.isupper() or mark in ('+', '-', '!'):
            cls = cls_base + '-problem'
        elif mark == '✎':
            cls = cls_base + '-info'
        return (cls, mark)

    def get_long_help(self):
        result = [
            ('title', 'Commit flags:\n\n'),
            self.format_mark('C'), '  a code conflict (not a clean cherry pick)\n',
            self.format_mark('·'), '  a context conflict\n',
            self.format_mark('R'), '  RHEL only patch\n',
            self.format_mark('!'), '  no upstream reference\n',
            self.format_mark('?'), '  unknown upstream commit\n',
        ]
        for mark, desc, _ in self.pkg.get_marks_help():
            result.append(self.format_mark(mark))
            result.append('  {}\n'.format(desc))
        result.extend([
            self.format_mark('✎'), '  has an unsubmitted comment\n',
        ])
        return result

    def format_mark_list(self, marks, cls_base='list', include_empty=True):
        result = []
        for i in range(self.commits.num_marks):
            try:
                m = marks[i]
            except IndexError:
                m = ' '
            if m == ' ' and not include_empty:
                continue
            result.append(self.format_mark(m, cls_base))
        return result

    def format_commit_item(self, commit):
        digits = len(str(self.commits.count))
        if commit.is_type_with_index() or commit.type == 'removed':
            index = '' if commit.type == 'removed' else commit.index
            label = [
                ('list', '{:{}} '.format(index, digits)),
                ('list-sha', commit.get_sha(6)),
                ('list', ' '),
            ]
            label.extend(self.format_mark_list(commit.marks))
            label.append(('list', ' {}'.format(commit.name)))
        elif commit.type == 'suggested':
            label = [
                ('list', ' ' * (digits + 9 + self.commits.num_marks) + '└─ '),
                ('list-sub', commit.category),
                ('list', ' '),
                ('list-sha', commit.get_sha(12)),
                ('list', ' {}'.format(commit.name)),
            ]
        else:
            assert False
        return label

    def update_commit_item(self, commit):
        # When called from load_commits, the list items are not assigned
        # yet. Do not bother with updating the texts, it will be done from
        # switch_commits.
        if commit.list_item_widget:
            commit.list_item_widget.set_text(self.format_commit_item(commit))
            self.update_title(commit_changed=True)

    def load_mr(self, fetch=False):
        if fetch:
            self.fetch()
        popup = self.app.wait_popup('Loading the merge request...')

        # get the merge request
        try:
            self.mr = self.project.mr(self.mr_id)
            self.mr.fetch()
        except gitlab.NotFoundError:
            popup.stop()
            self.app.message_popup(self.quit, 'Merge request !{} was not found.'.format(self.mr_id))
            return
        try:
            if self.mr['diff_refs']['head_sha'] != self.repo.get_mr_head(self.mr_id):
                self.mr = None
        except KeyError:
            self.mr = None
        if not self.mr:
            popup.stop()
            self.app.confirm('git-fetch', 'The local git data is outdated. Refresh?',
                             self.load_mr, (True,), reject_callback=self.quit)
            return
        # prefetch the author
        self.mr.author
        self.mr.data['description'] = self.remove_diffstat(self.mr['description'])
        # load versions
        lab_versions = self.mr.versions()
        self.versions = [git.MRVersion(self.repo, v, base_sha, i+1, len(lab_versions))
                         for i, (v, base_sha) in enumerate(lab_versions)]
        self.version_diffs = [None] * (len(self.versions) - 1)
        # load activity
        self.activity = mr_activity.Activity(self.repo, self.mr, self.versions)
        # postprocess versions
        for v in self.versions:
            v.set_shas(self.project, self.mr, self.activity)
        self.latest_version = self.versions[-1]
        self.action_tracker.set_action_enabled('compare-versions', len(self.versions) > 1)

        self.app.add_to_history(popup, self.mr, 'opened', minor=True)
        popup.stop()

        # data presentation
        self.mr_parse_info()
        self.commits = self.upstream_diff = self.mr_get_disp_commit_list()
        self.activity.build_widgets(self.commits)
        self.adjust_ui_mr_loaded()
        self.load_comments()
        self.switch_commits()

    def switch_commits(self, ver=None):
        self.save_comments()
        self.current_version = ver
        if ver is None:
            self.commits = self.upstream_diff
        else:
            ver -= 1
            if self.version_diffs[ver] is None:
                self.version_diffs[ver] = self.mr_get_disp_commit_list(self.versions[ver])
            self.commits = self.version_diffs[ver]
        self.layout.notify_commit_list_items(len(self.commits))
        walker = ui.PrettyListWalker((self.format_commit_item(c) for c in self.commits),
                                     self.app.focus_map, self.app.passive_map)
        for c, w in zip(self.commits, walker):
            c.set_list_item_widget(w.original_widget)
        urwid.connect_signal(walker, 'focus-changed', self.show_commit)
        self.widgets.commit_list.body = walker

        self.set_submit_comments_action()
        self.update_title(mr_changed=True, commit_changed=True)
        self.show_commit()
        if self.upstream_diff.file_list:
            self.action_tracker.set_action_enabled('find-file-next', ver is None)
            self.action_tracker.set_action_enabled('find-file-prev', ver is None)

    def load_comments(self):
        self.comment_storage = storage.CommentStorage(self.repo, self.mr['iid'])

        self.comment_count = 0
        self.action_tracker.set_action_enabled('submit-comments', False)

        oid_map = { c.get_sha(): c for c in self.upstream_diff
                                   if c.type == 'backport' }
        upstream_map = { c.upstream: c for c in self.upstream_diff
                                       if c.type == 'backport' and c.upstream }
        name_map = { c.name: c for c in self.upstream_diff
                               if c.type == 'backport' }

        general = None
        orphaned = []
        orphaned_fmt = formatters.CommentFormatter(self.pkg)
        for data in self.comment_storage:
            if data.get('commit'):
                # comment on a specific commit
                commit = None
                changed = False
                if data['commit'] in oid_map:
                    commit = oid_map[data['commit']]
                elif data.get('upstream') in upstream_map:
                    # the previous commit id is not present anymore, try to
                    # match on upstream commit id
                    commit = upstream_map[data['upstream']]
                    changed = True
                elif data['subject'] in name_map:
                    # nope, try to match on commit name
                    commit = name_map[data['subject']]
                    changed = True
                if commit is not None:
                    pos = data.get('position', {})
                    line_pos = (pos.get('old_path'), pos.get('new_path'),
                                pos.get('old_line'), pos.get('new_line'))
                    comment_handler = mr_activity.NewCommitActivityItem(commit, line_pos,
                                                                        self.repo)
                    try:
                        edit = self._add_comment(comment_handler,
                                                 comment_storage_id=data['id'])
                        edit.set_text(data['text'])
                        edit.blocking.set_state(data.get('blocking', False))
                        edit.draft.set_state(data.get('draft', False))
                    except mr_ui.ThreadAddError:
                        commit = None
                if commit is None:
                    # no luck, append as a general MR comment
                    text = orphaned_fmt.format(data['commit'], data['subject'], data['text'],
                                               upstream=data.get('upstream'),
                                               prefix='from a previous version')
                    orphaned.append((data, text))
                    continue
                if changed:
                    self._save_comment(edit)
            elif data.get('thread_id') == 'new':
                # new thread in the MR; handle as the last one in order to
                # add orphaned comments
                general = data
            elif data.get('thread_id'):
                # reply to a MR thread
                try:
                    comment_handler = self.activity.find_thread(data['thread_id'])
                    edit = self._add_comment(comment_handler,
                                             comment_storage_id=data['id'])
                    edit.set_text(data['text'])
                    edit.blocking.set_state(data.get('blocking', False))
                    edit.draft.set_state(data.get('draft', False))
                except mr_activity.ThreadNotFoundError:
                    text = orphaned_fmt.format(None, 'reply to a deleted thread', data['text'])
                    orphaned.append((data, text))
            else:
                assert False
        if orphaned and not general:
            # create an empty comment
            general = { 'thread_id': 'new', 'text': '', 'id': None }
        if general:
            for data, text in orphaned:
                general['text'] = orphaned_fmt.join((general['text'], text))
                general['blocking'] = (general.get('blocking', False) or 
                                       data.get('blocking', False))
                general['draft'] = (general.get('draft', False) or
                                       data.get('draft', False))
            edit = self._add_comment(self.activity.general_comment_handler,
                                     comment_storage_id=general['id'])
            edit.set_text(general['text'])
            edit.blocking.set_state(general.get('blocking', False))
            edit.draft.set_state(general.get('draft', False))
            if orphaned:
                self.comment_storage.save(general)
                for data, _ in orphaned:
                    del self.comment_storage[data]

    def save_compare_position(self, x, y):
        try:
            commit = self.commits[self.widgets.commit_list.focus_position]
        except (AttributeError, IndexError):
            # fired during init, ignore
            return
        commit.compare_position = (x, y)

    def set_submit_comments_action(self):
        enabled = (self.commits == self.upstream_diff and
                   self.comment_count > len(self.comments_drafts))
        self.action_tracker.set_action_enabled('submit-comments', enabled)

    def update_comment_count(self, commit, added):
        if added:
            self.comment_count += 1
            commit.comment_count += 1
        else:
            self.comment_count -= 1
            commit.comment_count -= 1
        self.set_submit_comments_action()

        new_mark = '✎' if commit.comment_count else ' '
        if commit.get_mark(self.upstream_diff.num_marks - 1) == new_mark:
            return

        commit.set_mark(self.upstream_diff.num_marks - 1, new_mark)
        self.update_commit_item(commit)

    def comment_changed(self, edit):
        if edit.draft.state:
            if edit not in self.comments_drafts:
                self.comments_drafts.add(edit)
                self.set_submit_comments_action()
        else:
            if edit in self.comments_drafts:
                self.comments_drafts.discard(edit)
                self.set_submit_comments_action()
        if edit not in self.comments_to_save:
            self.comments_to_save.append(edit)
        if hasattr(edit, 'deleted'):
            # process deletes immediatelly
            self.save_comments()
            self.comments_drafts.discard(edit)
            self.update_comment_count(self.upstream_diff[0], False)
            commit = edit.comment_handler.get_commit()
            if commit:
                self.update_comment_count(commit, False)

    def _save_comment(self, edit):
        data = {
            'text': edit.text,
            'blocking': edit.blocking.state,
            'draft': edit.draft.state,
        }
        if hasattr(edit, 'comment_storage_id'):
            data['id'] = edit.comment_storage_id
        data.update(edit.comment_handler.get_save_data())
        if hasattr(edit, 'deleted'):
            del self.comment_storage[data]
        else:
            self.comment_storage.save(data)
            edit.comment_storage_id = data['id']

    def save_comments(self):
        for edit in self.comments_to_save:
            self._save_comment(edit)
        self.comments_to_save.clear()
        # return True to rearm the timer
        return True

    def _submit_comments(self, button=None):
        if button is None or button == 'no':
            return

        if button == 'preview':
            self.app.view_popup(self.submit_comments, self.activity.preview_comments())
            return

        popup = self.app.wait_popup('Submitting comments...')
        keep_todo = settings['mr']['keep-todo']
        if keep_todo is not False:
            was_todo = self.mr.is_todo()
        self.activity.submit_comments(self.mr)
        self.app.add_to_history(popup, self.mr, 'added a comment')
        if keep_todo is True and was_todo:
            self._silent_mr_todo()
        popup.stop()
        self.update_cover()
        if keep_todo is None and was_todo:
            self.app.buttons_popup(self.readd_todo, 'Keep this MR in the TODO list?',
                                   ('~yes', '~no', '~Always', '~Never'),
                                   allow_esc=True)

    def submit_comments(self):
        self.save_comments()
        self.app.buttons_popup(self._submit_comments,
                               'Submit the comments to GitLab?',
                                ('~yes', '~preview', '~no'),
                                allow_esc=True)

    def _silent_mr_todo(self):
        try:
            self.mr.todo()
        except gitlab.NotModifiedError:
            pass

    def readd_todo(self, button=None):
        if button in (None, 'no'):
            return
        if button in ('yes', 'Always'):
            popup = self.app.wait_popup('Working...')
            self._silent_mr_todo()
            popup.stop()
        if button in ('Always', 'Never'):
            settings['mr']['keep-todo'] = (button == 'Always')
            settings.save()

    def _compare_versions(self, text=None):
        if text is None:
            return
        if text == '':
            self.switch_commits()
            return
        try:
            ver = int(text)
        except ValueError:
            ver = 0
        if ver < 1 or ver >= len(self.versions):
            self.app.message_popup(self.compare_versions, 'Invalid version',
                                   callback_args=(text,))
            return
        self.switch_commits(ver)

    def compare_versions(self, text='', **kwargs):
        end = len(self.versions) - 1
        self.app.edit_popup(self._compare_versions,
                            'Version to compare (1-{}, empty to upstream):'.format(end),
                            edit_text=text, allow_esc=True)

    def update_cover(self):
        self.set_commit_show_data(0, 'annot', self.mr_format_cover_annotation())

    def approve(self, approve=True):
        popup = self.app.wait_popup('Working...')
        if approve:
            try:
                self.mr.approve(self.latest_version.head_sha)
            except gitlab.ConflictError:
                popup.stop()
                self.app.message_popup(None, 'The MR got force-pushed meanwhile. Not approving.')
                return
        else:
            self.mr.unapprove()
        self.update_cover()
        self.app.add_to_history(popup, self.mr, 'approved' if approve else 'unapproved')
        popup.stop()

    def assign(self, assign=True):
        popup = self.app.wait_popup('Working...')
        self.mr.assign(assign)
        self.update_cover()
        self.app.add_to_history(popup, self.mr,
                                'assigned to self' if approve else 'unassigned self')
        popup.stop()

    def add_todo(self):
        popup = self.app.wait_popup('Working...')
        try:
            self.mr.todo()
        except gitlab.NotModifiedError:
            popup.stop()
            self.app.message_popup(None, 'Already on the TODO list.')
            return
        self.app.add_to_history(popup, self.mr, 'added to TODO list')
        popup.stop()

    def update_title(self, mr_changed=False, commit_changed=False):
        new_title_mode = self.layout.get_title_mode()
        if (new_title_mode == self.title_mode and
                not (self.title_mode == 'mr' and mr_changed) and
                not (self.title_mode == 'commit' and commit_changed)):
            return
        self.title_mode = new_title_mode

        marks = None
        if self.title_mode == 'commit':
            c = self.commits[self.widgets.commit_list.focus_position]
            if c.is_type_with_index():
                commits = '{}/{}'.format(c.index, self.commits.count)
                marks = self.format_mark_list(c.marks, 'header', include_empty=False)
            elif c.type == 'removed':
                commits = 'removed'
                marks = self.format_mark_list(c.marks, 'header', include_empty=False)
            else:
                assert c.type == 'suggested'
                commits = c.category
            name = c.name
        else:
            commits = '{} commit{}'.format(self.commits.count,
                                           '' if self.commits.count == 1 else 's')
            name = utils.oneline(self.mr['title'])
        if self.current_version:
            version = 'v{}→v{}'.format(self.current_version, self.latest_version.number)
        elif self.latest_version.number > 1:
            version = 'v{}'.format(self.latest_version.number)
        else:
            version = None

        title = [
            ('header', ' !{} '.format(self.mr['iid'])),
            ('header-branch', ' {}/{} '.format(self.repo.short_project, self.mr['target_branch'])),
        ]
        if version:
            title.append(('header-version', ' {} '.format(version)))
        title.append(('header-commits', ' {} '.format(commits)))
        if marks:
            title.append(('header', ' '))
            title.extend(marks)
        title.append(('header', ' {} '.format(name)))
        self.app.set_header(title)

    def set_all_content(self, commit):
        for placeholder, widget in zip(self.widgets.content, commit.widgets):
            placeholder.original_widget = widget

    def set_commit_show_data(self, index, content_type, data):
        widget = self.commits[index].get_widget(content_type)
        if widget:
            widget.set_text(data)

    def show_commit(self):
        index = self.widgets.commit_list.focus_position
        commit = self.commits[index]
        self.set_all_content(commit)
        if commit.has_comparison():
            pos = getattr(commit, 'compare_position', (0, 0))
            self.widgets.content_backport.set_text(commit.comparison[0], pos)
            self.widgets.content_upstream.set_text(commit.comparison[1], pos)
            self.action_tracker.set_action_enabled('interdiff', True)
            self.layout.enable_content_compare(True)
        else:
            self.widgets.content_backport.set_text('')
            self.widgets.content_upstream.set_text('')
            self.action_tracker.set_action_enabled('interdiff', False)
            self.layout.enable_content_compare(False)
        self.save_comments()
        self.activity.show_commit_info(commit.type == 'cover')
        if index > 0:
            commit.show_threads(self.show_commit_comments)
        self.action_tracker.set_action_enabled('comment', commit.is_type_with_index())
        self.action_tracker.set_action_enabled('new-thread', index == 0)
        self.action_tracker.set_action_enabled('full-activity', index == 0)
        self.action_tracker.set_action_enabled('hide-comments', index > 0)
        self.app.update_help()
        self.update_title(commit_changed=True)

    def scroll_commits(self, amount):
        try:
            self.widgets.commit_list.focus_position += amount
        except IndexError:
            pass

    def switch_to_content_compare(self):
        self.layout.switch_to_content_compare()
        self.update_title()

    def _add_comment(self, comment_handler, focus_widget=None, comment_storage_id=None):
        edit, new = self.activity.add_comment_editor(comment_handler, focus_widget,
                                                     self.app.highlight_map)
        if new:
            if comment_storage_id:
                assert not hasattr(edit, 'comment_storage_id')
                edit.comment_storage_id = comment_storage_id
            urwid.connect_signal(edit, 'changed', self.comment_changed,
                                 weak_args=(edit,))
            urwid.connect_signal(edit.blocking, 'changed', self.comment_changed,
                                 weak_args=(edit,))
            urwid.connect_signal(edit.draft, 'changed', self.comment_changed,
                                 weak_args=(edit,))

            actions = [
                { 'name': 'insert-template', 'prio': 5, 'help': 'template',
                  'long_help': 'insert a template text' },
            ]
            self.action_tracker.add_actions(edit, actions)
            self.action_tracker.set_action_enabled('insert-template',
                                                   bool(self.template_keys))

            self.update_comment_count(self.upstream_diff[0], True)
            commit = comment_handler.get_commit()
            if commit:
                self.update_comment_count(commit, True)
        return edit

    def add_comment(self, new_thread=False):
        if new_thread:
            comment_handler = self.activity.general_comment_handler
            focus_widget = None
        else:
            for focus, _ in reversed(self.app.workplace.get_focus_path_w(full=True)):
                if hasattr(focus, 'comment_handler'):
                    comment_handler = focus.comment_handler
                    focus_widget = focus
                    break
            else:
                return
        editor = self._add_comment(comment_handler, focus_widget=focus_widget)
        if editor:
            self.app.workplace.focus_to(editor, deep=True)

    def hide_comments(self):
        self.show_commit_comments = not self.show_commit_comments
        commit = self.commits[self.widgets.commit_list.focus_position]
        commit.show_threads(self.show_commit_comments)

    def search_start(self, **kwargs):
        self.app.edit_popup(self._search_new, 'Search for (regex)', self.last_search_input,
                            allow_esc=True)

    def _search_new(self, text=None):
        if not text:
            return
        self.last_search_input = text
        try:
            self.last_search = re.compile(text)
        except re.error as e:
            self.app.message_popup(self.search_start,
                                   'Invalid regular expression: {}'.format(e.msg))
            return
        self._search_next()

    def _search_next(self):
        focus = self.app.workplace.get_focus()
        if not focus:
            return
        if not focus.search(self.last_search):
            self.app.message_popup(None, 'Not found.')

    def search_next(self):
        if self.last_search:
            self._search_next()
        else:
            self.search_start()

    def search_next_diff(self, prev=False):
        commit = self.commits[self.widgets.commit_list.focus_position]
        content = self.widgets.content_compare.focus
        if content == self.widgets.content_backport:
            idx = 0
        elif content == self.widgets.content_upstream:
            idx = 2
        else:
            return
        x, y = content.text_pos_to_str_pos(*content.get_pref_text_cursor(bounded=True))
        jump_list = commit.comparison[2]
        if prev:
            jump_list = reversed(jump_list)
        for jump in jump_list:
            new_x = jump[idx]
            new_y = jump[idx + 1]
            if prev:
                if new_y > y or (new_y == y and new_x >= x):
                    continue
            else:
                if new_y < y or (new_y == y and new_x <= x):
                    continue
            content.set_pref_text_cursor(*content.text_str_pos_to_pos(new_x, new_y))
            return

    def show_templates(self, widget, size):
        self.template_target = (weakref.ref(widget), size)
        self.app.popup_show(self.widgets.template_popup)

    def insert_template(self):
        index = self.widgets.templates.focus_position
        self.widgets.template_popup.stop()
        widget, size = self.template_target
        widget = widget()
        if widget:
            key = self.template_keys[index]
            widget.insert_text(size, self.pkg.get_template(key, self.upstream_diff))
        self.template_target = None

    def show_find_file(self, backwards=False):
        self.app.popup_show(self.widgets.file_popup)
        self.find_file_backwards = backwards

    def find_file(self):
        path = self.upstream_diff.file_list[self.widgets.files.focus_position]
        self.widgets.file_popup.stop()
        if self.find_file_backwards:
            finish = -1
            step = -1
        else:
            finish = len(self.commits)
            step = 1
        start = self.widgets.commit_list.focus_position + step
        for i in range(start, finish, step):
            if self.commits[i].touches_file(path):
                break
        else:
            self.app.message_popup(None, 'No more matches.')
            return
        self.widgets.commit_list.focus_position = i

    def action(self, what, widget, size):
        if what == 'quit':
            self.quit()
        elif what == 'next-item':
            self.scroll_commits(1)
        elif what == 'prev-item':
            self.scroll_commits(-1)
        elif what == 'to-content':
            self.layout.switch_to_commit()
            self.update_title()
        elif what == 'from-content':
            self.layout.switch_from_content()
        elif what == 'to-commits':
            self.layout.switch_to_commit_list()
            self.update_title()
        elif what == 'bugzilla':
            for b in self.info['bugzilla']:
                self.app.web_browser(b)
        elif what == 'gitlab':
            self.app.web_browser(self.mr['web_url'])
        elif what == 'fullscreen':
            self.layout.switch_to_fullscreen()
            self.update_title()
        elif what == 'interdiff':
            self.switch_to_content_compare()
        elif what == 'comment':
            self.add_comment()
        elif what == 'new-thread':
            self.add_comment(new_thread=True)
        elif what == 'hide-comments':
            self.hide_comments()
        elif what == 'compare-versions':
            self.compare_versions()
        elif what == 'compare-last-approved-version':
            self._compare_versions(self.info['approved_version'])
        elif what == 'submit-comments':
            self.submit_comments()
        elif what == 'approve':
            self.approve(True)
        elif what == 'unapprove':
            self.approve(False)
        elif what == 'assign':
            self.assign(True)
        elif what == 'unassign':
            self.assign(False)
        elif what == 'todo-add':
            self.add_todo()
        elif what == 'ci-status':
            mr_ci.MRCIStatus(self.app, self.mr).fetch()
        elif what == 'find-file-next':
            self.show_find_file()
        elif what == 'find-file-prev':
            self.show_find_file(backwards=True)
        elif what == 'confirm-find-file':
            self.find_file()
        elif what == 'full-activity':
            self.activity.toggle()
        elif what == 'search':
            self.search_start()
        elif what == 'search-next':
            self.search_next()
        elif what == 'diff-next':
            self.search_next_diff()
        elif what == 'diff-prev':
            self.search_next_diff(prev=True)
        elif what == 'insert-template':
            self.show_templates(widget, size)
        elif what == 'confirm-template':
            self.insert_template()
