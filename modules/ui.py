import os
import signal
import urwid
from urwid.command_map import (CURSOR_LEFT, CURSOR_RIGHT, CURSOR_UP,
                               CURSOR_DOWN, CURSOR_MAX_LEFT, CURSOR_MAX_RIGHT,
                               CURSOR_PAGE_UP, CURSOR_PAGE_DOWN, CURSOR_TOP,
                               CURSOR_BOTTOM)

from modules.settings import settings


class clipboard:
    text = None


class EnhancedMainLoop(urwid.MainLoop):
    def __init__(self, *args, on_start=(), **kwargs):
        super().__init__(*args, **kwargs)
        self._on_start = on_start
        self.catch_signals()

    def catch_signals(self):
        def defer_to_sync(signalnum, stack):
            os.write(pipe, b'\0')

        def sigtstp(data):
            self.process_input(('ctrl z',))

        pipe = self.watch_pipe(sigtstp)
        self.event_loop.set_signal_handler(signal.SIGTSTP, defer_to_sync)

    def start(self):
        result = super().start()
        for func in self._on_start:
            func()
        return result


class MaxMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.max_size = None

    def set_max_size(self, index, max_size):
        """Set the max width/height of the widget at index. Only a single
        widget can have max size. Note that when redistributing the extra
        space, the weights are not taken into account. If we ever start
        using the weights, this would have to be fixed."""
        self.max_size = (index, max_size)
        self._invalidate()

    def adjust_size(self, sizes):
        if not self.max_size:
            return sizes

        index, max_size = self.max_size
        if sizes[index] <= max_size:
            return sizes

        to_enlarge = []
        for i, (_, s) in enumerate(self.contents):
            if i != index and s[0] not in (urwid.widget.GIVEN, urwid.widget.PACK):
                to_enlarge.append(i)

        add, rest = divmod(sizes[index] - max_size, len(to_enlarge))
        for i in to_enlarge:
            sizes[i] += add
            if rest:
                sizes[i] += 1
                rest -= 1
        sizes[index] = max_size
        return sizes


class ColumnsMax(MaxMixin, urwid.Columns):
    def column_widths(self, size, focus=False):
        widths = self.adjust_size(super().column_widths(size, focus))
        self._cache_column_widths = widths
        return widths


class PileMax(MaxMixin, urwid.Pile):
    def get_item_rows(self, size, focus):
        return self.adjust_size(super().get_item_rows(size, focus))


class Workplace(urwid.WidgetPlaceholder):
    signals = ['focus', 'help']

    def refocus(self):
        try:
            self.original_widget.base_widget.focus_position = 0
        except IndexError:
            return
        urwid.emit_signal(self, 'focus')

    def is_traversable(self, widget):
        """ListBox and TextBox are non-traversable. Meaning we always
        consider them as leaf when walking focus path. We do not descend
        into their content."""
        return not isinstance(widget, (urwid.ListBox, urwid.TextBox))

    def get_focus_path_w(self, deep=False, full=False):
        """A version of get_focus_path that returns also the widgets, that
        is, a list of tuples (widget, focus position in that widget).
        If deep is True, descend into non-traversable widgets. If
        full is True, include also decorations and the leaf widget in the
        result. The decorations and the leaf widget have None as the
        position. Setting full implies also deep."""
        if full:
            deep = True
        out = []
        w = self.original_widget
        while True:
            if not full:
                # skip decorations
                w = w.base_widget
            if not deep and not self.is_traversable(w):
                return out
            try:
                p = w.focus_position
            except IndexError:
                if full:
                    out.append((w, None))
                    if hasattr(w, 'original_widget'):
                        # this is a decoration, traverse it
                        w = w.original_widget
                        continue
                return out
            out.append((w, p))
            w = w.focus

    def set_focus_path_w(self, path, to_last=False):
        """A version of set_focus_path that accepts incomplete path. After
        path is consumed, it continues focusing widgets to their first
        selectable child (or the last selectable child if to_last is True).
        The focus continuation does not descend into non-traversable
        widgets."""
        w = self.original_widget.base_widget
        for _, p in path:
            if p != w.focus_position:
                w.focus_position = p
            w = w.focus.base_widget
        while True:
            if not self.is_traversable(w):
                return
            try:
                i = len(w.contents) - 1 if to_last else 0
                while True:
                    w.focus_position = i
                    if w.focus.selectable():
                        break
                    i = i - 1 if to_last else i + 1
            except (IndexError, AttributeError):
                return
            w = w.focus.base_widget

    def find_path_w(self, widget, deep=False):
        """Search for the given widget in the whole tree. If deep is True,
        descend into non-traversable widgets. Return the path to the widget
        (may be []) or None if the widget was not found."""
        path = []
        w, pos = self.original_widget.base_widget, 0
        while True:
            try:
                child = w.contents[pos][0]
            except (IndexError, TypeError, AttributeError):
                if len(path) == 0:
                    return None
                w, pos = path.pop()
                pos += 1
                continue
            while True:
                if child == widget:
                    path.append((w, pos))
                    return path
                if not hasattr(child, 'original_widget'):
                    break
                child = child.original_widget
            if not child.selectable():
                pos += 1
                continue
            if not deep and not self.is_traversable(child):
                pos += 1
                continue
            path.append((w, pos))
            w, pos = child, 0

    def focus_next(self, prev=False):
        path = self.get_focus_path_w()
        done = False
        while len(path):
            w, p = path[-1]
            try:
                while True:
                    p = p - 1 if prev else p + 1
                    path[-1] = (w, p)
                    self.set_focus_path_w(path, prev)
                    if w.focus.selectable():
                        urwid.emit_signal(self, 'focus')
                        return
            except IndexError:
                path = path[:-1]
        self.set_focus_path_w(path, prev)
        urwid.emit_signal(self, 'focus')

    def focus_to(self, widget, deep=False):
        path = self.find_path_w(widget, deep=deep)
        if path is not None:
            self.set_focus_path_w(path)
            urwid.emit_signal(self, 'focus')
            return True
        return False

    def get_focus(self, deep=False):
        try:
            w, _ = self.get_focus_path_w(deep=deep)[-1]
        except IndexError:
            return None
        return w.focus.base_widget

    def get_help(self, long=False):
        data = {}
        for w, _ in reversed(self.get_focus_path_w(full=True)):
            w._command_map.collect_help(data)

        if not data:
            return ''

        result = []
        used_keys = set(settings.key_map['help'])
        if not long:
            result.append(('help-key', ' {} '.format(settings.key_map['help'][0])))
            result.append(('help-desc', ' help  '))
        for prio in sorted(data.keys(), reverse=True):
            for enabled, all_keys, desc, long_desc in data[prio]:
                if enabled:
                    keys = [key for key in all_keys if key not in used_keys]
                    used_keys.update(keys)
                    if not keys:
                        enabled = False
                if not enabled:
                    keys = all_keys
                if not long and not desc:
                    continue
                if not long and not enabled:
                    continue
                if long and not long_desc:
                    continue
                if long:
                    for i, key in enumerate(keys):
                        if i > 0:
                            result.append(('help-desc', ', '))
                        result.append(('help-key', ' {} '.format(key)))
                    state = '' if enabled else ' [disabled]'
                    result.append(('help-desc', ' {}{}\n'.format(long_desc, state)))
                else:
                    result.append(('help-key', ' {} '.format(keys[0])))
                    result.append(('help-desc', ' {}  '.format(desc)))
        return result

    def keypress(self, size, key):
        key = super().keypress(size, key)
        if key in settings.key_map['focus_next'] and self._command_map._all_enabled:
            self.focus_next()
            return None
        if key in settings.key_map['focus_prev'] and self._command_map._all_enabled:
            self.focus_next(prev=True)
            return None
        if key in settings.key_map['help'] and self._command_map._all_enabled:
            urwid.emit_signal(self, 'help', self.get_help(long=True) or [])
            return None
        return key


class PassiveAttrMap(urwid.AttrMap):
    def __init__(self, w, attr_map, focus_map, passive_map):
        self._passive = False
        super().__init__(w, attr_map, focus_map)
        self.set_passive_map(passive_map)

    def set_attr_map(self, attr_map):
        super().set_attr_map(attr_map)
        self._orig_attr_map = self._attr_map
        if self._passive:
            self._attr_map = self._passive_map

    def set_passive_map(self, passive_map):
        orig = self._focus_map
        super().set_focus_map(passive_map)
        self._passive_map = self._focus_map
        self._focus_map = orig

    def set_passive(self, enabled):
        if self._passive == enabled:
            return
        if enabled:
            self._attr_map = self._passive_map
        else:
            self._attr_map = self._orig_attr_map
        self._passive = enabled
        self._invalidate()


class PrettyListWalker(urwid.SimpleFocusListWalker):
    def __init__(self, content, focus_map, passive_map):
        self._focus_map = focus_map
        self._passive_map = passive_map
        items = [self._build_item(c) for c in content]
        self._ext_callback = None
        super().__init__(items)
        if self.focus is not None:
            self._set_passive(self.focus, True)

    def _build_item(self, c):
        w = urwid.SelectableIcon(c)
        w.set_wrap_mode('ellipsis')
        return PassiveAttrMap(w, 'list', self._focus_map, self._passive_map)

    def append(self, item):
        super().append(self._build_item(item))

    def _set_passive(self, index, enabled):
        try:
            widget = self[index]
        except IndexError:
            assert not enabled
            # this may happen when we're refocusing a changed list
            return
        if hasattr(widget, 'set_passive'):
            widget.set_passive(enabled)

    def _focus_changed(self, new_focus):
        self._set_passive(self.focus, False)
        self._set_passive(new_focus, True)
        if self._ext_callback:
            self._ext_callback(new_focus)

    def set_focus_changed_callback(self, callback):
        self._ext_callback = callback


class JailedPlaceholder(urwid.WidgetPlaceholder):
    """A placeholder widget that does not allow arrow keys to escape. Used
    as a container for widgets that should be focused using the Tab key
    only."""
    def keypress(self, size, key):
        key = super().keypress(size, key)
        if self._command_map[key] in (CURSOR_LEFT,
                                      CURSOR_RIGHT,
                                      CURSOR_UP,
                                      CURSOR_DOWN,
                                      CURSOR_MAX_LEFT,
                                      CURSOR_MAX_RIGHT,
                                      CURSOR_PAGE_UP,
                                      CURSOR_PAGE_DOWN):
            key = None
        return key


class FixedButton(urwid.Button):
    def pack(self, size, focus=False):
        return (len(self._label.text) + 4, 1)


class ToggleCheckBox(urwid.CheckBox):
    def keypress(self, size, key):
        if key not in settings.key_map['toggle']:
            return key
        self.toggle_state()


class Popup(urwid.Overlay):
    def __init__(self, callback, workplace, attr_map=None, focus_map=None, height=None,
                 callback_args=None, popup_widget=None, overlay_attr_map=None):
        self._workplace = workplace
        self._attr_map = attr_map
        self._focus_map = focus_map
        self._callback = callback
        self._callback_args = callback_args or ()
        self._default_button = None
        self._default_esc_button = None
        self._shortcuts = {}
        self._data_widgets = {}
        if height == 'max':
            parms = { 'align': 'center',
                      'width': ('relative', 50),
                      'valign': 'middle',
                      'height': ('relative', 100),
                    }
        elif height == 'full':
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': ('relative', 100),
                    }
        elif height is None:
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': 'pack',
                    }
        else:
            if type(height) == int:
                height = height + 2
            else:
                assert(height == 'pack')
            parms = { 'align': 'center',
                      'width': ('relative', 100),
                      'valign': 'middle',
                      'height': ('relative', 0),
                      'min_height': height,
                    }
        super().__init__(None, self._workplace.original_widget,
                         attr_map=overlay_attr_map, **parms)
        if popup_widget:
            self._setup(popup_widget)

    def new_button(self, label):
        if type(label) == str:
            index = label.find('~')
            if index >= 0:
                shortcut = label[index + 1]
                label = [label[:index], ('button-shortcut', shortcut), label[index+2:]]
                self._shortcuts[shortcut] = urwid.util.decompose_tagmarkup(label)[0]
        button = FixedButton(label)
        urwid.connect_signal(button, 'click', self._handler)
        return urwid.AttrMap(button, self._attr_map, self._focus_map)

    def new_edit(self, name, edit_text):
        widget = urwid.Editor(edit_text)
        self._data_widgets[name] = widget
        return widget

    def register_widget(self, name, widget):
        self._data_widgets[name] = widget
        return widget

    def set_default_button(self, label, label_esc=None):
        """Sets the button that is reported when Enter is pressed in one of
        the edit boxes. Similarly for Esc. Note that the label does not need
        to correspond to a real button. As a special case, if label or
        label_esc is True, the Enter/Esc is functional but the button label
        is not reported to the callback. Edit data is reported to the
        callback only if a non-Esc button was pressed."""
        self._default_button = label
        self._default_esc_button = label_esc

    def _setup(self, widget):
        self.popup_widget = widget
        self.top_w = urwid.LineBox(widget)

    def show(self):
        self._workplace.original_widget = self
        self._workplace._command_map.set_all_actions_enabled(False)

    def start(self, widget):
        self._setup(widget)
        self.show()

    def _stop(self):
        self._workplace.original_widget = self.contents[0][0]
        self._workplace._command_map.set_all_actions_enabled(True)

    def stop(self):
        self._stop()
        self._callback(*self._callback_args)

    def keypress(self, size, key):
        if key == 'enter':
            focus = self.get_focus_widgets()
            handle = not focus
            for f in focus:
                f = f.base_widget
                if f in self._data_widgets.values():
                    handle = f
                    break
            if handle and self._handler(key=key, focus=f if handle is not True else None):
                return None
        elif key == 'esc':
            if self._handler(key=key):
                return None
        elif key in self._shortcuts:
            if self._handler(key=key):
                return None
        return super().keypress(size, key)

    def _handler(self, button=None, key=None, focus=None):
        collect_data = True
        if button is not None:
            button = urwid.util.decompose_tagmarkup(button.label)[0]
            if button == self._default_esc_button:
                collect_data = False
        if key is not None:
            if key == 'enter' and self._default_button:
                button = self._default_button
            elif key == 'esc' and self._default_esc_button:
                button = self._default_esc_button
                collect_data = False
            elif key in self._shortcuts:
                button = self._shortcuts[key]
                if button == self._default_esc_button:
                    collect_data = False
            else:
                return False

        self._stop()
        data = {}
        if collect_data:
            for k, widget in self._data_widgets.items():
                if isinstance(widget, urwid.Editor):
                    data[k] = widget.get_text()
                elif isinstance(widget, urwid.Pile) and widget == focus:
                    data[k] = widget.focus_position
                elif isinstance(widget, urwid.ListBox):
                    data[k] = widget.body[widget.focus_position].original_widget.text
        if button is not None and button is not True:
            data['button'] = button
        self._callback(*self._callback_args, **data)
        return True
