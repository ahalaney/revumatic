import urwid

from modules import ui

class LayoutBase:
    """Base class for layout definition.
    Class attributes:
    fullscreen_supported - True if the layout supports the fullscreen action.
    content_enter_supported - True if the layout does something with the
                              Enter key pressed in a content widget.
    """
    fullscreen_supported = False
    content_enter_supported = False

    def __init__(self, workplace, commit_list, content_compare):
        self.workplace = workplace
        self.commit_list = commit_list
        self.content_compare = content_compare

    def get_content_defs(self):
        """You may redefine this in your subclass. It should return tuple or
        list defining what to show in content widgets. Each item in
        content_defs is a tuple of values to show. The available values are:

        "annot" - include annotation (meta data) about the commit/cover
                  letter.
        "header" - include commit description/cover letter text.
        "stat" - include diffstat.
        "disc" - include discussion; non-empty only for the cover letter.
        "diff" - include the diff; empty for the cover letter.
        "diff_all" - the diff of the whole MR; non-empty only for the cover
        letter.

        No more than one widget can have "disc" and no more than one widget
        can have "diff"."""
        return (('annot', 'header', 'stat', 'diff', 'disc'),)

    def setup(self, *args):
        """Redefine in your subclass. It gets content widgets as parameters.
        It should arrange all the widgets (those passed to __init__ and
        those pasedd to setup) and return a single widget that will be used
        as the workplace."""
        raise NotImplementedError

    def notify_commit_list_items(self, number):
        """You may redefine this in your subclass if you're interested in
        the number of items in the commit list, once it is known."""
        return

    def enable_content_compare(self, enable):
        """Redefine in your subclass. It should enable or disable the
        content compare widget. When called with False, this method must
        hide the widget. When called with True, the method may show the
        widget or keep it hidden, depending on the layout needs."""
        raise NotImplementedError

    def switch_to_commit(self):
        """Redefine in your subclass. It is called as a reaction to user
        selecting a commit from the commit list. The method should switch
        focus to the commit details. It is up to the layout to decide what
        "commit details" means; it could be any of the content or compare
        widgets."""
        raise NotImplementedError

    def switch_to_commit_list(self):
        """Redefine in your subclass. It is called as a reaction to user
        pressing Esc in the content, compare or commit widgets. The method
        should switch to the commit list, showing the list if it was
        previously hidden."""
        raise NotImplementedError

    def switch_to_content_compare(self):
        """Redefine in your subclass. It is called as a reaction to the 'u'
        key. It should switch focus to the content comparison, showing the
        widgets if they were previously hidden, or switch the focus from the
        content comparison. You can assume that this will never be called if
        a previous call to enable_content_compare disabled the widgets."""
        raise NotImplementedError

    def switch_to_fullscreen(self):
        """Redefine in your subclass if you set fullscreen_supported to
        True. Otherwise, you can leave this not implemented. It is called as
        a reaction to the 'f' key. It should switch some widget (usually the
        focused one but it's not required) to or from fullscreen mode."""
        raise NotImplementedError

    def switch_from_content(self):
        """If you set content_enter_supported, you must redefine this in
        your subclass. It is called whenever the Enter key is pressed in any
        of the content widgets."""
        raise NotImplementedError

    def get_title_mode(self):
        """You may redefine this in your subclass. It gets called to obtain
        the mode of the title bar. Available return values are 'mr' and
        'commit', for details about the merge request or details about the
        selected commit, respectively. The 'mr' value is useful when the
        commit list is visible, the 'commit' value when it is hidden."""
        return 'mr'


class StandardLayout(LayoutBase):
    fullscreen_supported = True
    commit_list_max = 25

    def create_panels(self):
        return ui.PileMax((
            self.top,
            ('pack', urwid.AttrMap(urwid.Divider('─'), 'divider')),
            self.bottom))

    def setup(self, content):
        self.content = content
        self.top = ui.JailedPlaceholder(self.commit_list)
        self.bottom = ui.JailedPlaceholder(self.content)
        self.bottom_mode = 0
        self.panels = self.create_panels()

        self.main = urwid.WidgetPlaceholder(self.panels)
        self.fullscreen = False
        self.commit_list_items = None

        return self.main

    def notify_commit_list_items(self, number):
        new_size = self.commit_list_max
        if number is not None:
            self.commit_list_items = number
            new_size = min(number, new_size)
        self.panels.set_max_size(0, new_size)

    def _set_bottom_mode(self, mode):
        if mode == 0:
            self.bottom.original_widget = self.content
        else:
            self.bottom.original_widget = self.content_compare

    def _switch_fullscreen(self, force_off=False):
        if not self.fullscreen and force_off:
            return
        if self.fullscreen:
            self.main.original_widget = self.panels
        else:
            self.main.original_widget = self.bottom
        self.fullscreen = not self.fullscreen

    def enable_content_compare(self, enable):
        self._set_bottom_mode(self.bottom_mode if enable else 0)

    def switch_to_commit(self):
        self.workplace.focus_to(self.bottom)

    def switch_to_commit_list(self):
        self._switch_fullscreen(force_off=True)
        self.workplace.focus_to(self.commit_list)

    def switch_to_content_compare(self):
        self.bottom_mode = 0 if self.bottom_mode == 1 else 1
        self._set_bottom_mode(self.bottom_mode)

    def switch_to_fullscreen(self):
        self._switch_fullscreen()

    def get_title_mode(self):
        if self.fullscreen:
            return 'commit'
        return 'mr'


class WideLayout(StandardLayout):
    def create_panels(self):
        panels = ui.ColumnsMax((
            self.top,
            (1, urwid.AttrMap(urwid.SolidFill('│'), 'divider')),
            self.bottom))
        panels.set_max_size(0, 80)
        return panels

    def notify_commit_list_items(self, number):
        return

class LargeLayout(LayoutBase):
    content_enter_supported = True

    def get_content_defs(self):
        return (('annot', 'header', 'stat', 'disc'),
                ('diff', 'diff_all'))

    def setup(self, content, diff):
        self.content = content
        self.diff = diff
        self.left = ui.PileMax((
            ui.JailedPlaceholder(self.commit_list),
            ('pack', urwid.AttrMap(urwid.Divider('─'), 'divider')),
            ('weight', 2, ui.JailedPlaceholder(self.content)),
            ))
        self.right = ui.JailedPlaceholder(self.content_compare)
        self.main = ui.ColumnsMax((
            self.left,
            (1, urwid.AttrMap(urwid.SolidFill('│'), 'divider')),
            self.right))
        self.main.set_max_size(0, 80)
        return self.main

    def notify_commit_list_items(self, number):
        self.left.set_max_size(0, min(number, 25))

    def enable_content_compare(self, enable):
        if enable:
            self.right.original_widget = self.content_compare
        else:
            self.right.original_widget = self.diff

    def switch_to_commit(self):
        self.workplace.focus_to(self.content)

    def switch_to_commit_list(self):
        self.workplace.focus_to(self.commit_list)

    def switch_to_content_compare(self):
        self.content_compare.focus_position = 1 - self.content_compare.focus_position

    def switch_from_content(self):
        if self.workplace.get_focus() == self.content:
            self.workplace.focus_to(self.right)
