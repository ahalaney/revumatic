Name:    revumatic
Version: 22.05.04
Release: 1%{?dist}
Summary: The TUI tool for CentOS/RHEL kernel review.

License: GPLv2+
URL:     https://gitlab.com/redhat/centos-stream/src/kernel/utils/revumatic
Source:  %{url}/-/archive/v%{version}/revumatic-v%{version}.tar.bz2

BuildArch:     noarch
BuildRequires: python3-devel

Requires: python3-pygit2
Requires: python3-Levenshtein
Requires: python3-urllib3

%description
A tool that eases review of CentOS Stream and RHEL kernel merge requests on
GitLab.

%prep
%autosetup -n revumatic-v%{version}

%build

%install
install -d %{buildroot}%{python3_sitelib}/revumatic
install -T revumatic %{buildroot}%{python3_sitelib}/revumatic/revumatic.py
install -d %{buildroot}%{python3_sitelib}/revumatic/modules
install -m 644 -t %{buildroot}%{python3_sitelib}/revumatic/modules modules/*.py
install -d %{buildroot}%{python3_sitelib}/revumatic/urwid
install -m 644 -t %{buildroot}%{python3_sitelib}/revumatic/urwid urwid/*.py
install -d %{buildroot}%{_bindir}
ln -s %{python3_sitelib}/revumatic/revumatic.py %{buildroot}%{_bindir}/revumatic

install -d %{buildroot}%{_pkgdocdir}
install -m 644 -t %{buildroot}%{_pkgdocdir} README.md

%files
%{_bindir}/revumatic
%{python3_sitelib}/revumatic
%{_pkgdocdir}

%changelog
* Wed May 4 2022 Jiri Benc <jbenc@redhat.com> - 22.05.04-1
- version update

* Thu Mar 24 2022 Jiri Benc <jbenc@redhat.com> - 22.03.24-1
- initial packaging
