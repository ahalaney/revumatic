import bisect
import itertools
import re

from urwid import canvas, container, monitored_list, signals, widget
from urwid.command_map import (command_map, CURSOR_LEFT, CURSOR_RIGHT,
    CURSOR_UP, CURSOR_DOWN, CURSOR_MAX_LEFT, CURSOR_MAX_RIGHT,
    CURSOR_PAGE_UP, CURSOR_PAGE_DOWN, CURSOR_TOP, CURSOR_BOTTOM,
    CURSOR_WORD_LEFT, CURSOR_WORD_RIGHT, SELECTION_START, SELECTION_CANCEL,
    SELECTION_COPY, PASTE, DELETE_LINE, UNDO, REDO)

# Unicode "fullwidth" characters. These are printed as double width in the console.
FULLWIDTH = re.compile(r'[\u1100-\u115f\u231a-\u231b\u2329-\u232a\u23e9-\u23ec\u23f0-\u23f0\u23f3-\u23f3\u25fd-\u25fe\u2614-\u2615\u2648-\u2653\u267f-\u267f\u2693-\u2693\u26a1-\u26a1\u26aa-\u26ab\u26bd-\u26be\u26c4-\u26c5\u26ce-\u26ce\u26d4-\u26d4\u26ea-\u26ea\u26f2-\u26f3\u26f5-\u26f5\u26fa-\u26fa\u26fd-\u26fd\u2705-\u2705\u270a-\u270b\u2728-\u2728\u274c-\u274c\u274e-\u274e\u2753-\u2755\u2757-\u2757\u2795-\u2797\u27b0-\u27b0\u27bf-\u27bf\u2b1b-\u2b1c\u2b50-\u2b50\u2b55-\u2b55\u2e80-\u2e99\u2e9b-\u2ef3\u2f00-\u2fd5\u2ff0-\u2ffb\u3000-\u3004\u3006-\u3029\u3030-\u3030\u3036-\u303a\u303c-\u303e\u3041-\u3096\u309b-\u309c\u309f-\u30fb\u30ff-\u30ff\u3105-\u312f\u3131-\u318e\u3190-\u31e3\u31f0-\u321e\u3220-\u3247\u3250-\u4dbf\u4e00-\ua014\ua016-\ua48c\ua490-\ua4c6\ua960-\ua97c\uac00-\ud7a3\uf900-\ufa6d\ufa70-\ufad9\ufe10-\ufe19\ufe30-\ufe52\ufe54-\ufe66\ufe68-\ufe6b\uff01-\uff60\uffe0-\uffe6\U00016fe2-\U00016fe2\U00017000-\U000187f7\U00018800-\U00018cd5\U00018d00-\U00018d08\U0001b000-\U0001b122\U0001b150-\U0001b152\U0001b164-\U0001b167\U0001b170-\U0001b2fb\U0001f004-\U0001f004\U0001f0cf-\U0001f0cf\U0001f18e-\U0001f18e\U0001f191-\U0001f19a\U0001f200-\U0001f202\U0001f210-\U0001f23b\U0001f240-\U0001f248\U0001f250-\U0001f251\U0001f260-\U0001f265\U0001f300-\U0001f320\U0001f32d-\U0001f335\U0001f337-\U0001f37c\U0001f37e-\U0001f393\U0001f3a0-\U0001f3ca\U0001f3cf-\U0001f3d3\U0001f3e0-\U0001f3f0\U0001f3f4-\U0001f3f4\U0001f3f8-\U0001f43e\U0001f440-\U0001f440\U0001f442-\U0001f4fc\U0001f4ff-\U0001f53d\U0001f54b-\U0001f54e\U0001f550-\U0001f567\U0001f57a-\U0001f57a\U0001f595-\U0001f596\U0001f5a4-\U0001f5a4\U0001f5fb-\U0001f64f\U0001f680-\U0001f6c5\U0001f6cc-\U0001f6cc\U0001f6d0-\U0001f6d2\U0001f6d5-\U0001f6d7\U0001f6dd-\U0001f6df\U0001f6eb-\U0001f6ec\U0001f6f4-\U0001f6fc\U0001f7e0-\U0001f7eb\U0001f7f0-\U0001f7f0\U0001f90c-\U0001f93a\U0001f93c-\U0001f945\U0001f947-\U0001f9ff\U0001fa70-\U0001fa74\U0001fa78-\U0001fa7c\U0001fa80-\U0001fa86\U0001fa90-\U0001faac\U0001fab0-\U0001faba\U0001fac0-\U0001fac5\U0001fad0-\U0001fad9\U0001fae0-\U0001fae7\U0001faf0-\U0001faf6\U00020000-\U0002a6df\U0002a700-\U0002b738\U0002b740-\U0002b81d\U0002b820-\U0002cea1\U0002ceb0-\U0002ebe0\U0002f800-\U0002fa1d\U00030000-\U0003134a]')


fullwidth_supported = True
def disable_fullwidth():
    global fullwidth_supported
    fullwidth_supported = False


class TagMarkupException(Exception):
    pass

class TagAttr(int):
    def __init__(self, offset, attr):
        super().__init__(offset)
        self.attr = attr

def decompose_tagmarkup(tm, tab_width=8, tab_offset=0, keep_end_nl=False, default_attr=None):
    """Return (text lines, attribute lines) for tagmarkup passed. Unlike the
    original urwid's decompose_tagmarkup, does tab and fullwidth char expansion.
    Also, the return value is different. A list of strings, one for each line,
    and a list of corresponding attributes, one list for each line, is returned.
    The attribute list contains (attr, offset) tuples.

    If keep_end_nl is True and the text ends with a new line, there will be
    an additional empty line in the returned lists."""

    def expand_tab(match):
        nonlocal extra_chars

        add = tab_width - (tab_offset + extra_chars + match.start()) % tab_width - 1
        result = '\t' + '\x01' * add
        extra_chars += add
        return result

    def add(text, attr):
        nonlocal cur_attr, offset, extra_chars

        text = text.split('\n')
        for i, t in enumerate(text):
            if i > 0:
                text_list.append(''.join(cur_text))
                attr_list.append(cur_attr)
                cur_text.clear()
                cur_attr = [(attr, 0)]
                offset = 0

            extra_chars = 0
            if not t:
                continue
            if fullwidth_supported:
                t = FULLWIDTH.sub(lambda match: match.group() + '\x00', t)
            else:
                t = FULLWIDTH.sub('\ufffd', t)
            t = re.sub('\t', expand_tab, t)
            cur_text.append(t)
            if cur_attr[-1][0] != attr:
                # do not repeat the attr if it's the same as the previous one
                if cur_attr[-1][1] == offset:
                    # overwrite a zero length attr
                    cur_attr[-1] = (attr, offset)
                else:
                    cur_attr.append((attr, offset))
            offset += len(t)

    tab_offset %= tab_width
    text_list = []
    attr_list = []
    cur_text = []
    cur_attr = [(default_attr, 0)]
    offset = 0
    extra_chars = 0
    stack = [(default_attr, tm, 0)]

    while stack:
        attr, current, index = stack.pop()

        if isinstance(current, str):
            add(current, attr)
            continue

        if isinstance(current, tuple):
            if len(current) != 2:
                raise TagMarkupException("Tuples must be in the form (attribute, tagmarkup): {}".format(current))
            stack.append((current[0], current[1], 0))
            continue

        if isinstance(current, list):
            if index >= len(current):
                continue
            stack.append((attr, current, index + 1))
            stack.append((attr, current[index], 0))
            continue

        raise TagMarkupException("Invalid markup element: {}".format(current))

    if cur_text or not text_list or keep_end_nl:
        # If there's unprocessed data (meaning there was no \n at the end of
        # tm), flush them; also flush the (empty) data if tm was empty.
        # If keep_end_nl is True, always flush.
        add('\n', None)
    return text_list, attr_list


TAB_RE = re.compile(r'\t\x01*')


def retab(text, attr, tab_width=8, tab_offset=0):
    """Fixes tab widths in the given text. Returns a new (text, attr)."""

    def copy_attrs(pos):
        nonlocal attr_pos

        while attr_pos < len(attr) and attr[attr_pos][1] <= pos:
            nattr.append((attr[attr_pos][0], attr[attr_pos][1] + extra_chars))
            attr_pos += 1

    def expand_tab(match):
        nonlocal extra_chars

        copy_attrs(match.start())
        add = tab_width - (tab_offset + extra_chars + match.start()) % tab_width - 1
        result = '\t' + '\x01' * add
        extra_chars += add - len(match.group()) + 1
        return result

    extra_chars = 0
    attr_pos = 0
    nattr = []
    ntext, count = TAB_RE.subn(expand_tab, text)
    if not count:
        return text, attr
    copy_attrs(len(text))
    return ntext, nattr


def split_text_attr_line(text, attr, pos):
    """Splits the given 'text' (a string representing a single line) and its
    corresponding 'attr' at the 'pos'. Returns a tuple of (ltext, lattr, rtext,
    rattr)."""
    lattr = []
    rattr = []
    if pos == 0:
        # need to have at least one attr even for empty text
        lattr.append((None, 0))
    i = 0
    while i < len(attr) and attr[i][1] < pos:
        lattr.append(attr[i])
        i += 1
    if i == len(attr) or attr[i][1] > pos:
        # splitting in the middle of an attr; duplicate the attr
        rattr.append((attr[i-1][0], 0))
    while i < len(attr):
        # also adjust the offsets
        rattr.append((attr[i][0], attr[i][1] - pos))
        i += 1
    return text[:pos], lattr, text[pos:], rattr


def combine_text_attr_line(text1, attr1, text2, attr2):
    """Combines the given texts and their attrs to a single line. Returns
    a tuple of (text, attr). BEWARE, it modifies attr1."""
    i = 0
    if attr1[-1][0] == attr2[0][0]:
        # attr1 ends with the same attr that attr2 starts with; combine them
        i = 1
    add = len(text1)
    while i < len(attr2):
        attr1.append((attr2[i][0], attr2[i][1] + add))
        i += 1
    return text1 + text2, attr1


class clipboard:
    text = None


class TextBase(widget.Widget):
    _sizing = frozenset([widget.FLOW])
    signals = ['moved', 'changed', 'selectable-changed', 'selection']

    def __init__(self, padding_char=' ', default_attr=None):
        self._padding_char = padding_char
        self._default_attr = default_attr
        self._in_column = False

    def subrender(self, size, origin_y, focus=False):
        """
        Render part of the contents. Return canvas of the exact size (both
        columns and rows) starting at origin_y.
        """
        c = canvas.SolidCanvas(self._padding_char, *size)
        if self._default_attr:
            c = canvas.CompositeCanvas(c)
            c.fill_attr_apply({ None: self._default_attr })
        return c

    def render(self, size, focus=False):
        """
        Render contents with wrapping and alignment.  Return canvas.

        See :meth:`Widget.render` for parameter details.

        Note that due to caching imposed by the WidgetMeta metaclass,
        subclasses must redefine the `render` method as:

        def render(self, size, focus=False):
            return super().render.original_fn(self, size, focus)
        """
        return self.subrender((size[0], self.rows(size, focus)), 0, focus)

    def rows(self, size, focus=False):
        """
        Return the number of rows the rendered text requires.

        See :meth:`Widget.rows` for parameter details.
        """
        return 1

    def active_rows(self, size):
        """
        Return the first and last row+1 at which the cursor can be. For
        widgets that do not have inactive but rendered area at the top or
        bottom, this is the same as (0, rows()).
        """
        return 0, self.rows(size)

    def text_lines(self):
        """
        Return the number of lines of (unwrapped) text.
        """
        return 1

    def set_in_column(self, enable):
        """
        Inform the widget it is rendered in a column, alongside other text
        widgets. The widget can change its behavior based on this.
        """
        self._in_column = enable

    def _send_signal(self, name, *args):
        signals.emit_signal(self, name, *args)


class Text(TextBase):
    """
    This is similar to the original urwid's Text widget and is mostly
    a drop-in replacement, except that it does not support a layout
    instance. It however supports tabs correctly and is more performant.

    The cursor position is expressed (and kept) in two different type of
    coordinates: text position and rendered position.

    Text position is coordinates to the `markup`: considering the `markup`
    split to lines, the y coordinate is line number and x coordinate is
    a char on the given line. Both are counted from 0.

    Rendered position is coordinates to the text after wrapping the lines.
    This does not take into account a viewport (e.g. what part of the widget
    is actually visible). The very first line of the widget has y coordinate
    0 and the very first char on a line has x coordinate 0. For widgets in
    'clip' or 'ellipsis' wrap mode (i.e. those that do not wrap lines),
    rendered position and text position are equal.

    Both cursor text and rendered positions have also they "preferred"
    instance. This is the desired position of the cursor. The x coordinate
    may be after the last character on the line or it can be a "left" or
    "right" string to denote the maximum left or right position available.
    The preferred x coordinate (unlike the real x coordinate) may also fall
    in the middle of a wide character, such as a tab, or to the left padding
    for right aligned lines. The y coordinate may be before or after the
    last line.
    """

    ignore_focus = True
    # Allow cursor to be after the last character on the line? Set to True
    # in editable subclasses.
    cursor_at_eol = False

    def __init__(self, markup, align='left', wrap='space', padding_char=' ',
                 cursor_position=('left', 0), tab_width=8, tab_offset=0,
                 default_attr=None):
        """
        :param markup: content of text widget, one of:

            str
              text to be displayed

            (*display attribute*, *text markup*)
              *text markup* with *display attribute* applied to all parts
              of *text markup* with no display attribute already applied

            [*text markup*, *text markup*, ... ]
              all *text markup* in the list joined together

        :type markup: :ref:`text-markup`
        :param align: ``'left'``, ``'center'`` or ``'right'``
        :type align: text alignment mode
        :param wrap: ``'space'``, ``'any'``, ``'clip'`` or ``'ellipsis'``
        :type wrap: text wrapping mode
        :param padding_char: character to fill the alignment with
        :param cursor_position: a tuple with the preferred text position
        :param tab_width: spacing of tab stops
        :param tab_offset: number of characters that are pretended to be
        added before each line for the purpose of tab stops calculation
        :param default_attr: default attribute to use for otherwise
        undefined attributes (including the padding)
        """
        super().__init__(padding_char, default_attr)
        self._text = ()
        self._cache_maxcol = None
        self._origin_x = 0
        self._align_mode = align
        self._tab_width = tab_width
        self._tab_offset = tab_offset
        self.set_wrap_mode(wrap)
        self.set_text(markup, cursor_position)

    def _repr_words(self):
        """
        Show the text in the repr format and truncate if it's too long
        """
        words = super()._repr_words()
        text = repr(self.get_text())
        if len(text) > 80:
            text = '{}...{}'.format(text[:40], text[-40+3:])
        words.append(text)
        return words

    def _invalidate(self, how=None):
        """
        :param rebuild: if True, discard also the line wrap cache. Set if
        the text, line wrapping or widget width changed.
        """
        super()._invalidate()
        if how != 'moved':
            self.lines = None

    def _send_moved(self):
        self._send_signal('moved', *self.get_pref_text_cursor(bounded=True))

    def _apply_text_attr(self, cursor_position=None):
        self.min_full_width = max(len(t) for t in self._text)
        if cursor_position:
            self._cursor_text_pref = list(cursor_position)
            self._cursor_text_real_x = None
        self.highlight = None
        self._invalidate()
        self._send_signal('changed')

    def set_text(self, markup, cursor_position=None):
        """
        Set content of text widget.

        :param markup: see :class:`Text` for description.
        :type markup: text markup
        :param cursor_position: a tuple with the preferred text position
        """
        self._text, self._attr = decompose_tagmarkup(markup,
                                                     tab_width=self._tab_width,
                                                     tab_offset=self._tab_offset,
                                                     default_attr=self._default_attr)
        self._apply_text_attr(cursor_position)

    def get_text(self):
        """
        :returns: *text*

            *text*
              complete str content of text widget
        """
        # delete '\x00' and '\x01' chars
        return '\n'.join(s.translate([ None, None ]) for s in self._text)

    text = property(lambda self: self.get_text(), doc="""
        Read-only property returning the complete str content
        of this widget
        """)

    def set_align_mode(self, mode):
        """
        Set text alignment mode.

        :param mode: ``'left'``, ``'center'`` or ``'right'``
        :type mode: text alignment mode
        """
        if mode not in ('left', 'center', 'right'):
            raise ValueError('Unrecognized value for align: '.format(mode))
        self._align_mode = mode
        self._stable_cache = self._wrap_mode in ('clip', 'ellipsis') and mode == 'left'
        self._invalidate()
        self._send_signal('changed')

    def set_wrap_mode(self, mode):
        """
        Set text wrapping mode.

        :param mode: ``'space'``, ``'any'``, ``'clip'`` or ``'ellipsis'``
        :type mode: text wrapping mode
        """
        if mode not in ('space', 'any', 'clip', 'ellipsis'):
            raise ValueError('Unrecognized value for wrap: '.format(mode))
        self._wrap_mode = mode
        self.set_align_mode(self._align_mode)

    align = property(lambda self: self._align_mode,
                     lambda self, value: self.set_align_mode(value))
    wrap = property(lambda self: self._wrap_mode,
                    lambda self, value: self.set_wrap_mode(value))

    def _attr_offset_advance(self, line_index, start, attr_offs):
        """
        Returns an offset into self._attr[line_index] that corresponds to
        the text position `start`. The searching starts at `attr_offs`.
        """
        attr = self._attr[line_index]
        while attr_offs < len(attr) - 1:
            if attr[attr_offs + 1][1] > start:
                break
            attr_offs +=1
        return attr_offs

    def _highlight_offsets(self, size, y, start, end):
        """
        Returns the offsets into self._text[y] where the highlight starts
        and ends on that line. The return value is a list with three fields:
        highlight start, highlight start excluding cursor, highlight end.

        This allows applying a different attribute to the character at the
        cursor. Rendering it the same as the rest of the highlight looks
        weird.

        If there's no highlight on the line, returns None.
        """
        if not self.highlight:
            return None
        hstart, hend = self.highlight
        if hstart[1] > y or hend[1] < y:
            return None
        res = [start, start, end]
        if hstart[1] == y:
            if hstart[0] >= end:
                return None
            res[0] = res[1] = max(start, hstart[0])
        if hend[1] == y:
            if hend[0] <= start:
                return None
            res[2] = min(end, hend[0])

        if start < end:
            # only on a non-empty line
            cx, cy = self.get_text_cursor(size)
            if cy == y and cx == res[1] and cx != res[2]:
                res[1] += 1
        return res

    def build_line_cache(self, maxcol):
        # The cache is a list of lines. Each line is a tuple of (line index
        # in _text/_attr, offset in the line, end offset in the line, offset
        # to the attr in the line, spaces at the beginning).
        # If spaces at the beginning are < 0, the beginning of the line was
        # clipped.
        if self.lines is not None and (self._stable_cache or self._cache_maxcol == maxcol):
            return False
        self._cache_maxcol = maxcol
        self.lines = []
        cursor_x = self._cursor_text_real_x
        if cursor_x is None:
            cursor_x = self._cursor_text_pref[0]
        if cursor_x == 'left':
            cursor_x = 0
        cursor_y = min(max(0, self._cursor_text_pref[1]), len(self._text) - 1)
        if self._wrap_mode in ('clip', 'ellipsis'):
            self.min_width = 0
            # in these modes, cache line == line
            rendered_cursor_y = cursor_y
        else:
            self.min_width = maxcol

        for i, t in enumerate(self._text):
            if self._wrap_mode in ('clip', 'ellipsis'):
                if self._align_mode == 'left':
                    spaces = 0
                    offset = 0
                    attr_offs = 0
                else:
                    spaces = maxcol - len(t)
                    if self._align_mode == 'center':
                        spaces //= 2
                    offset = 0 if spaces >= 0 else -spaces
                    attr_offs = self._attr_offset_advance(i, offset, 0)
                self.lines.append((i, offset, len(t), attr_offs, spaces))
                if spaces < 0:
                    spaces = 0
                self.min_width = max(self.min_width, len(t) - offset + spaces)
                continue

            # self._wrap_mode in ('any', 'space'):
            start = 0
            attr_start = 0
            if i == cursor_y:
                rendered_cursor_y = len(self.lines)    # default for cursor_x < 0
            while True:
                skip_next = 0
                end = start + maxcol
                if self.cursor_at_eol:
                    end -= 1
                if end >= len(t):
                    end = len(t)
                else:
                    if self._wrap_mode == 'space':
                        old_end = end
                        while end > start:
                            if t[end] in (' ', '\t'):
                                skip_next = 1
                                break
                            end -= 1
                        else:
                            # no space, break arbitrarily
                            end = old_end
                    if t[end] == '\x00':
                        # cannot split double width character in half
                        end -= 1
                while start < end and t[start] == '\x01':
                    # eat tab continuation from the beginning of line
                    start += 1
                if start >= end:
                    # Nothing is left from the line. We still render it as
                    # an empty sub-line if:
                    # - it's the sole sub-line (meaning the line is empty), or
                    # - we're an editor or similar (cursor_at_eol is True).
                    # Otherwise, we skip it.
                    if start > 0 and not self.cursor_at_eol:
                        if start >= len(t):
                            break
                        continue
                attr_start = self._attr_offset_advance(i, start, attr_start)
                if self._align_mode == 'left':
                    spaces = 0
                else:
                    spaces = maxcol - (end - start)
                    if self._align_mode == 'center':
                        spaces //= 2
                self.lines.append((i, start, end, attr_start, spaces))
                if cursor_y == i and (cursor_x == 'right' or cursor_x >= start):
                    rendered_cursor_y = len(self.lines) - 1
                if end >= len(t):
                    break
                # Process one more line even if end + skip_next == len(t).
                # That happens when the last line ended with a space. It
                # still means an empty sub-line may need to be rendered.
                start = end + skip_next

        max_x = self._get_max_rendered_cursor_x(rendered_cursor_y)
        _, start, _, _, spaces = self.lines[rendered_cursor_y]
        if cursor_x == 'right':
            x = max_x
        else:
            self._cursor_rendered_pref_x = cursor_x - start + spaces
            x = min(max(0, self._cursor_rendered_pref_x), max_x)
        self._cursor_rendered = self._adjust_rendered_cursor(x, rendered_cursor_y)
        if isinstance(self._cursor_text_pref[0], str):
            self._cursor_rendered_pref_x = self._cursor_text_pref[0]
        return True

    def _add_attr_subset(self, dest, y, start, end, attr_start):
        # get the substring attrs; the ellipses are rendered with the same
        # attr as the character they replace
        while attr_start < len(self._attr[y]) - 1:
            next_offs = self._attr[y][attr_start + 1][1]
            if next_offs <= start:
                # skip the attrs at the beginning; can happen only if
                # origin_x > 0 or for highlights
                attr_start += 1
                continue
            if next_offs >= end:
                # we fit completely
                dest.append((self._attr[y][attr_start][0], end-start))
                start = end
                break
            # consume the attr and shift to the next one
            dest.append((self._attr[y][attr_start][0], next_offs-start))
            start = next_offs
            attr_start +=1
        if start < end:
            # the last attr spans to the end of line
            dest.append((self._attr[y][attr_start][0], end-start))

    def subrender(self, size, origin_y, focus=False):
        """
        Render part of the contents. Return canvas of the exact size (both
        columns and rows) starting at origin_y.
        """
        cols, rows = size
        self.build_line_cache(cols)

        # shift the x origin if cursor is out of the rendered area
        if self._cursor_rendered[0] < self._origin_x:
            self._origin_x = self._cursor_rendered[0]
        elif self._cursor_rendered[0] >= self._origin_x + cols:
            self._origin_x = self._cursor_rendered[0] - cols + 1

        y = origin_y
        row_end = min(y + rows, len(self.lines))
        text = []
        attr = []
        while y < row_end:
            cur_text_line = []
            cur_attr_line = []
            padding_required = cols
            i, start, end, attr_start, spaces = self.lines[y]
            start += self._origin_x
            spaces -= self._origin_x
            skip_start = skip_end = 0
            # spaces at the beginning
            if spaces > 0:
                cur_text_line.append(self._padding_char * spaces)
                cur_attr_line.append((self._default_attr, spaces))
                padding_required -= spaces
            # ellipsis at the beginning if trimmed
            elif spaces < 0 and self._wrap_mode == 'ellipsis':
                cur_text_line.append('…')
                padding_required -= 1
                skip_start = 1
            # check if there's an ellipsis at the end
            if start+cols < end and self._wrap_mode == 'ellipsis':
                skip_end = 1
            # get the substring
            end = min(end, start+cols)
            s = self._text[i][start+skip_start:end-skip_end]
            cur_text_line.append(s)
            padding_required -= len(s)
            # add the ellipsis at the end
            if skip_end:
                cur_text_line.append('…')
                padding_required -= 1
            # add the attrs, accounting for the highlight
            hl = self._highlight_offsets(size, i, start, end) if focus else None
            if hl is None:
                self._add_attr_subset(cur_attr_line, i, start, end, attr_start)
            else:
                # pre-highlight
                if start < hl[0]:
                    self._add_attr_subset(cur_attr_line, i, start, hl[0], attr_start)
                # highlight + cursor
                if hl[0] < hl[1]:
                    cur_attr_line.append(('select-cursor', hl[1] - hl[0]))
                # highlight
                if hl[1] < hl[2]:
                    cur_attr_line.append(('select', hl[2] - hl[1]))
                # post-highlight
                if hl[2] < end:
                    # as an optimization, we could save attr_start from the
                    # pre-highlight run and continue where it left off; do
                    # not bother for now
                    self._add_attr_subset(cur_attr_line, i, hl[2], end, attr_start)
            # pad with spaces at the end
            if padding_required > 0:
                cur_text_line.append(self._padding_char * padding_required)
                cur_attr_line.append((self._default_attr, padding_required))

            text.append(''.join(cur_text_line))
            attr.append(cur_attr_line)
            y += 1
        # pad vertically
        row_end = origin_y + rows
        while y < row_end:
            text.append(self._padding_char * cols)
            attr.append([(self._default_attr, cols)])
            y += 1

        c = canvas.TextCanvas(text, attr, maxcol=cols, check_width=False)
        if focus:
            c.cursor = self.get_viewport_rendered_cursor(size, origin_y)
        return c

    def render(self, size, focus=False):
        return super().render.original_fn(self, size, focus)

    def rows(self, size, focus=False):
        """
        Return the number of rows the rendered text requires.

        See :meth:`Widget.rows` for parameter details.
        """
        self.build_line_cache(size[0])
        return len(self.lines)

    def text_lines(self):
        return len(self._text)

    def pack(self, size=None, focus=False):
        """
        Return the number of screen columns and rows required for
        this Text widget to be displayed without wrapping or
        clipping, as a single element tuple.

        :param size: ``None`` for unlimited screen columns or (*maxcol*,) to
                     specify a maximum column size
        :type size: widget size
        """
        if size is not None:
            self.build_line_cache(size[0])
            return (self.min_width, len(self.lines))
        return (self.min_full_width, len(self._text))

    def search(self, regex, from_cursor=True, backwards=False):
        """
        Return the text cursor position of the first match of the given
        regex. If from_cursor is False, the search begins at the beginning
        of the text (or end of the text for backwards=True). Otherwise, it
        starts after the current cursor position. Returns None on no match.
        """

        def re_rsearch(s, endpos):
            res = None
            for m in regex.finditer(s):
                if m.start() < endpos - 1:
                    res = m
                else:
                    break
            return res

        x, y = 0, 0
        if from_cursor:
            x, y = self.get_pref_text_cursor(bounded=True)
            x += 1
        elif backwards:
            y = len(self._text) - 1
            x = len(self._text[y])
        sy = 1 if not backwards else -1
        while y < len(self._text):
            func = regex.search if not backwards else re_rsearch
            match = func(self._text[y], x)
            if match:
                return match.start(), y
            y += sy
            if y < 0:
                break
            x = 0 if not backwards else len(self._text[y])
        return None

    # internal helper methods for cursor position

    def _skip_wide_left(self, x, ty, start, spaces):
        line = self._text[ty]
        if start + x - spaces >= len(line):
            # cursor at eol
            return x
        while x > spaces:
            if ord(line[start + x - spaces]) > 1:
                break
            x -= 1
        return x

    def _skip_wide_right(self, x, ty, start, end, spaces):
        for i in range(start + x - spaces, end):
            if ord(self._text[ty][i]) > 1:
                break
            x += 1
        return x

    def _adjust_rendered_cursor(self, x, y):
        """
        Adjusts the computed rendered cursor coordinates to not fall in
        a middle of a wide character and to not fall in the start padding.
        """
        ty, start, end, _, spaces = self.lines[y]
        spaces = max(0, spaces)
        if x < spaces:
            x = spaces
            if not self.cursor_at_eol and start == end:
                # empty line, padding only
                x -= 1
        else:
            x = self._skip_wide_left(x, ty, start, spaces)
        return [x, y]

    def _count_adjust_wide_char(self, x, y, to_right=False):
        """
        Count the number of characters that need to be added to the rendered
        cursor in order to not fall in a middle of a wide character.
        """
        ty, start, end, _, spaces = self.lines[y]
        spaces = max(0, spaces)
        if x < spaces:
            return 0
        if to_right:
            nx = self._skip_wide_right(x, ty, start, end, spaces)
        else:
            nx = self._skip_wide_left(x, ty, start, spaces)
        return nx - x

    def _get_min_rendered_cursor_x(self, y):
        """
        For the given rendered y coordinate, return the minimum rendered
        x coordinate possible.

        It will be non-zero only for right or center aligned lines.
        """
        _, _, _, _, spaces = self.lines[y]
        return max(0, spaces)

    def _get_max_rendered_cursor_x(self, y):
        """
        For the given rendered y coordinate, return the maximum rendered
        x coordinate possible.

        It's basically the rendered line length adjusted for the
        cursor_at_eol option.
        """
        ty, start, end, _, spaces = self.lines[y]
        spaces = max(0, spaces)
        max_x = end - start + spaces
        if not self.cursor_at_eol and max_x > 0:
            max_x = self._skip_wide_left(max_x - 1, ty, start, spaces)
        return max_x

    def _pref_text_cursor_to_rendered_cursor(self, x, y):
        """
        Calculate the rendered cursor from the given text cursor preferred
        coordinates. Expects a valid line cache.

        Returns a tuple of (cursor_rendered, cursor_rendered_pref_x).
        """
        orig_x = x
        if x == 'left':
            x = 0
        elif x == 'right':
            x = None
        y = min(max(0, y), len(self._text) - 1)
        try:
            rendered_y = bisect.bisect_left(self.lines, y, key=lambda line: line[0])
        except TypeError:
            # Python < 3.10 does not have the 'key' argument to bisect, use
            # a linear search:
            rendered_y = next(i for i, line in enumerate(self.lines) if line[0] == y)
        while rendered_y < len(self.lines) - 1:
            i, _, end, _, _ = self.lines[rendered_y]
            if i > y:
                rendered_y -= 1
                break
            if x is not None and x < end:
                break
            rendered_y += 1
        max_x = self._get_max_rendered_cursor_x(rendered_y)
        _, start, _, _, spaces = self.lines[rendered_y]
        if x is None:
            x = max_x
        else:
            cursor_rendered_pref_x = x - start + spaces
            x = min(max(0, cursor_rendered_pref_x), max_x)
        cursor_rendered = self._adjust_rendered_cursor(x, rendered_y)
        if isinstance(orig_x, str):
            cursor_rendered_pref_x = orig_x
        return cursor_rendered, cursor_rendered_pref_x

    def _get_text_cursor(self):
        """
        An internal version of get_text_cursor. Expects a valid line cache.
        """
        _, start, _, _, spaces = self.lines[self._cursor_rendered[1]]
        spaces = max(0, spaces)
        y = min(max(0, self._cursor_text_pref[1]), len(self._text) - 1)
        x = min(max(0, self._cursor_rendered[0] - spaces + start), len(self._text[y]))
        return x, y

    # methods for rendered cursor position

    def get_pref_rendered_cursor(self, size):
        """
        Return the preferred rendered cursor position.
        """
        self.build_line_cache(size[0])
        return (self._cursor_rendered_pref_x, self._cursor_rendered[1])

    def set_pref_rendered_cursor(self, size, x, y, adjust=None):
        """
        Move the cursor to the given preferred rendered position.

        If adjust is 'left' or 'right' and the cursor is in a middle of
        a wide char, the preferred position is adjusted to be at that char
        ('left') or to the right of it ('right'). By default, the preferred
        position is not adjusted and the cursor is rendered at the char.
        """
        self.build_line_cache(size[0])
        self._cursor_rendered_pref_x = x
        y = min(max(0, y), len(self.lines) - 1)
        max_x = self._get_max_rendered_cursor_x(y)
        i, start, _, _, spaces = self.lines[y]
        if x == 'left':
            nx = 0
        elif x == 'right':
            nx = max_x
        else:
            nx = min(max(0, x), max_x)
            x = x - spaces + start
            if adjust:
                delta = self._count_adjust_wide_char(nx, y, adjust=='right')
                nx += delta
                x += delta
                self._cursor_rendered_pref_x += delta
        self._cursor_rendered = self._adjust_rendered_cursor(nx, y)
        self._cursor_text_pref = [x, i]
        self._cursor_text_real_x = self._get_text_cursor()[0]
        self._invalidate('moved')
        self._send_moved()
        return True

    # methods for rendered cursor position - urwid compat, do not use

    def get_pref_col(self, size):
        """
        Return the x coordinate of the preferred rendered cursor position.
        For urwid compatibility, do not use in text widgets.
        """
        return self.get_pref_rendered_cursor(size)[0]

    def move_cursor_to_coords(self, size, x, y):
        """
        For urwid compatibility, do not use in text widgets.
        """
        return self.set_pref_rendered_cursor(size, x, y)

    # methods for text cursor position

    def get_text_cursor(self, size):
        """
        Return the actual (not the preferred) text cursor position.
        """
        self.build_line_cache(size[0])
        return self._get_text_cursor()

    def get_pref_text_cursor(self, bounded=False):
        """
        Return the preferred text cursor position.

        If `bounded` is True, the result will never be outside of the text.
        """
        x, y = self._cursor_text_pref
        if bounded:
            y = min(max(0, y), len(self._text) - 1)
            if x == 'left':
                x = 0
            elif x == 'right':
                x = len(self._text[y])
            else:
                x = min(max(0, x), len(self._text[y]))
        return (x, y)

    def set_pref_text_cursor(self, x, y):
        """
        Set the preferred text cursor position.
        """
        self._cursor_text_pref = (x, y)
        if self.lines is None:
            # No line cache, we're done. The next build_line_cache will take
            # care of recalculating the rendered cursor position.
            self._cursor_text_real_x = None
            self._send_moved()
            return
        # We could just call _invalidate() here and let the build_line_cache
        # take care of the recalculation but there's no point in wasting the
        # cpu cycles on rebuilding the line cache unnecessarily. Recalculate
        # the rendered cursor here.
        res = self._pref_text_cursor_to_rendered_cursor(x, y)
        self._cursor_rendered, self._cursor_rendered_pref_x = res
        self._send_moved()

    def text_pos_to_str_pos(self, x, y):
        """
        Given the (x, y) position in the text, return the (x, y) position in
        the original text passed to __init__ or set_text. This differs
        because of tab and double width char rendering.
        """
        # delete '\x00' and '\x01' chars
        line_part = self._text[y][:x+1].translate([ None, None ])
        x = max(0, len(line_part) - 1)
        return (x, y)

    def text_str_pos_to_pos(self, x, y):
        """
        Given the (x, y) position in the original text passed to __init__ or
        set_text, return the (x, y) position in the text. This differs
        because of tab and double width char rendering.
        """
        for i, ch in enumerate(self._text[y]):
            if ord(ch) <= 1:
                continue
            if x == 0:
                break
            x -= 1
        else:
            # set i just past the string, i.e. to len(self._text[y])
            i += 1
        return (i, y)

    # various methods for cursor position

    def unpref_cursor(self, size):
        """
        Sets the preferred cursor position to the current cursor position,
        for both the rendered and the text cursor.
        """
        self.build_line_cache(size[0])
        self._cursor_rendered_pref_x = self._cursor_rendered[0]
        self._cursor_text_pref = self._get_text_cursor()
        self._cursor_text_real_x = self._cursor_text_pref[0]

    def get_viewport_rendered_cursor(self, size, origin_y=0):
        """
        Return the position of the cursor *on the canvas*. If the cursor is
        not visible, return None.

        This is the only cursor method that takes the viewport into account.
        It returns the rendered cursor position adjusted for the viewport.
        """
        self.build_line_cache(size[0])
        x = self._cursor_rendered[0] - self._origin_x
        y = self._cursor_rendered[1] - origin_y
        if x < 0 or y < 0 or x >= size[0]:
            return None
        return x, y

    # various methods for adjusted cursor position - urwid compat, do not use

    def get_cursor_coords(self, size):
        """
        For urwid compatibility, do not use in text widgets.
        """
        return self.get_viewport_rendered_cursor(size)


class SelectableIcon(Text):
    """
    This is a text widget that is selectable.  A cursor
    displayed at a fixed location in the text when in focus.
    This widget has no special handling of keyboard or mouse input.
    """
    _selectable = True

    def __init__(self, text, cursor_position=('left', 0), **kwargs):
        """
        :param text: the markup to display
        :param cursor_position: the fixed cursor position
        Additionally, all parameters accepted by Text() (align, wrap, etc.)
        are accepted.
        """
        single_line = False
        if isinstance(cursor_position, int):
            # For a (limited) backwards compatibility with the urwid's
            # original SelectableIcon, support an integer cursor position.
            # But only for a single line of text.
            cursor_position = (cursor_position, 0)
            single_line = True
        super().__init__(text, cursor_position=cursor_position, **kwargs)
        if single_line and len(self._text) != 1:
            raise ValueError('cursor_position must be a tuple')
        self._fixed_text_cursor = cursor_position

    def build_line_cache(self, maxcol):
        if not super().build_line_cache(maxcol):
            return
        res = self._pref_text_cursor_to_rendered_cursor(*self._fixed_text_cursor)
        self._fixed_rendered_cursor = res[0]

    def render(self, size, focus=False):
        return super().render.original_fn(self, size, focus)

    def unpref_cursor(self, size):
        self.build_line_cache(size[0])
        self._cursor_rendered = self._fixed_rendered_cursor
        super().unpref_cursor(size)

    def get_viewport_rendered_cursor(self, size, origin_y=0):
        self.build_line_cache(size[0])
        x = self._fixed_rendered_cursor[0] - self._origin_x
        y = self._fixed_rendered_cursor[1] - origin_y
        if x < 0 or y < 0 or x >= size[0]:
            return None
        return x, y

    def keypress(self, size, key):
        """
        No keys are handled by this widget.  This method is
        required for selectable widgets.
        """
        return key


class TextSeparator(TextBase):
    """
    A horizontal separator, using the given character to fill the widget
    width.
    """
    ignore_focus = True

    def __init__(self, sep='─', default_attr=None):
        super().__init__(padding_char=sep, default_attr=default_attr)


class Viewer(Text):
    """
    This is a widget displaying text and allowing the user to move the
    cursor in the text (including text selection).
    """
    _selectable = True

    def render(self, size, focus=False):
        return super().render.original_fn(self, size, focus)

    def _is_cursor_at_line_start(self):
        x, y = self._cursor_rendered
        # x can be < minimum if the line is right aligned and empty
        return x <= self._get_min_rendered_cursor_x(y)

    def _is_cursor_at_line_end(self):
        x, y = self._cursor_rendered
        return x == self._get_max_rendered_cursor_x(y)

    def _move_cursor_vertically(self, size, y_delta):
        y = self._cursor_rendered[1] + y_delta
        if y < 0 or y > len(self.lines) - 1:
            return False
        return self.set_pref_rendered_cursor(size, self._cursor_rendered_pref_x, y)

    def _move_cursor_left(self, size):
        x, y = self._cursor_rendered
        if self._is_cursor_at_line_start():
            if self._in_column or y == 0:
                return False
            self.set_pref_rendered_cursor(size, 'right', y - 1)
        else:
            self.set_pref_rendered_cursor(size, x - 1, y, adjust='left')
        return True

    def _move_cursor_right(self, size):
        x, y = self._cursor_rendered
        if self._is_cursor_at_line_end():
            if self._in_column or y == len(self.lines) - 1:
                return False
            self.set_pref_rendered_cursor(size, 'left', y + 1)
        else:
            self.set_pref_rendered_cursor(size, x + 1, y, adjust='right')
        return True

    def _move_cursor_word(self, backwards=False):
        try:
            regex = self._word_start_regex
        except AttributeError:
            regex = self._word_start_regex = re.compile(r'\b(?=\w)')
        cursor = self.search(regex, backwards=backwards)
        if not cursor:
            return False
        self.set_pref_text_cursor(*cursor)
        return True

    def selection_start(self, size):
        x, y = self.get_text_cursor(size)
        self.highlight_origin = (x, y)
        self.highlight = ((x, y), (x,y))
        self._invalidate('moved')
        self._send_signal('selection', True)

    def selection_stop(self, size):
        self.highlight = None
        self._invalidate('moved')
        self._send_signal('selection', False)

    def set_text(self, markup, cursor_position=None):
        super().set_text(markup, cursor_position)
        # the set_text in the superclass clears the highlight, need to send
        # a signal
        self._send_signal('selection', False)

    def selection_update(self, size):
        if not self.highlight:
            return
        x, y = self.get_text_cursor(size)
        if y < self.highlight_origin[1] or (y == self.highlight_origin[1] and
                                            x < self.highlight_origin[0]):
            self.highlight = ((x, y), self.highlight_origin)
        else:
            self.highlight = (self.highlight_origin, (x, y))

    def selection_copy(self):
        if not self.highlight:
            return
        start, end = self.highlight
        x, y = start
        text = []
        while y <= end[1]:
            if y == end[1]:
                s = self._text[y][x:end[0]]
            elif x == 0:
                s = self._text[y]
            else:
                s = self._text[y][x:]
            # delete '\x00' and '\x01' chars
            text.append(s.translate([ None, None ]))
            x = 0
            y += 1
        clipboard.text = '\n'.join(text)

    def set_pref_rendered_cursor(self, size, x, y, adjust=None):
        result = super().set_pref_rendered_cursor(size, x, y, adjust)
        self.selection_update(size)
        return result

    def keypress(self, size, key):
        self.build_line_cache(size[0])
        height = size[1] if len(size) == 2 else 10
        ckey = self._command_map[key]
        if ckey == CURSOR_LEFT:
            if not self._move_cursor_left(size):
                return key
        elif ckey == CURSOR_RIGHT:
            if not self._move_cursor_right(size):
                return key
        elif ckey == CURSOR_UP:
            if not self._move_cursor_vertically(size, -1):
                return key
        elif ckey == CURSOR_DOWN:
            if not self._move_cursor_vertically(size, 1):
                return key
        elif ckey == CURSOR_MAX_LEFT:
            self.set_pref_rendered_cursor(size, 'left', self._cursor_rendered[1])
        elif ckey == CURSOR_MAX_RIGHT:
            self.set_pref_rendered_cursor(size, 'right', self._cursor_rendered[1])
        elif ckey == CURSOR_PAGE_UP:
            if not self._move_cursor_vertically(size, -height):
                return key
        elif ckey == CURSOR_PAGE_DOWN or key == ' ':
            if not self._move_cursor_vertically(size, height):
                return key
        elif ckey == CURSOR_TOP:
            self.set_pref_rendered_cursor(size, 'left', 0)
        elif ckey == CURSOR_BOTTOM:
            self.set_pref_rendered_cursor(size, 'right', len(self.lines) - 1)
        elif ckey == CURSOR_WORD_LEFT:
            if not self._move_cursor_word(backwards=True):
                return key
        elif ckey == CURSOR_WORD_RIGHT:
            if not self._move_cursor_word():
                return key
        elif ckey == SELECTION_START:
            self.selection_start(size)
        elif ckey == SELECTION_CANCEL:
            self.selection_stop(size)
        elif ckey == SELECTION_COPY:
            self.selection_copy()
            self.selection_stop(size)
        else:
            return key


class Editor(Viewer):
    """
    This is a text editing widget. It is generally not compatible with the
    urwid's original Edit widget, although it can directly replace it in
    simple cases.

    Some features of the Edit widget are not implemented, namely: allow_tab
    and mask is not supported (those can be easily added later). Caption is
    not supported. The constructor arguments have different names: edit_text
    -> markup, edit_pos -> cursor_position. There's no set_edit_text, the
    regular set_text should be used instead.

    However, unlike the Edit widget, Editor allows colored text to be
    edited. Copy/paste and undo is supported.
    """
    cursor_at_eol = True

    def __init__(self, *args, multiline=False, undo=False, **kwargs):
        self.multiline = multiline
        self.keep_undo = undo
        if len(args) == 0:
            args = ['']
        super().__init__(*args, **kwargs)
        if not multiline and len(self._text) > 1:
            raise ValueError('Need a single line in the non-multiline mode')

    def set_text(self, markup, cursor_position=None):
        self._undo_buffer = []
        self._undo_pos = -1
        self._undo_last_type = None
        super().set_text(markup, cursor_position)

    def _send_moved(self):
        self.add_undo(None)
        super()._send_moved()

    def _close_undo_slot(self):
        # Undo slot is open if _undo_pos points behind the last slot.
        if self._undo_pos == len(self._undo_buffer):
            self._undo_pos -= 1
        self._undo_last_type = None

    def _set_undo_slot(self, cur_type):
        assert self._undo_pos < len(self._undo_buffer)
        if self._undo_pos < len(self._undo_buffer) - 1:
            # ditch redo
            self._undo_buffer = self._undo_buffer[:self._undo_pos + 1]
        # self._text and self._attr are never modified in place and their
        # individual items are not modified in place, either. Instead,
        # a copy of the lists and of the modified items is done for every
        # modification. We can thus store directly the current lists.
        self._undo_buffer.append((self._text, self._attr, self.get_pref_text_cursor()))
        # keep the slot open
        self._undo_pos = len(self._undo_buffer)
        self._undo_last_type = cur_type

    def add_undo(self, cur_type):
        """
        Records an operation for undo. cur_type is 'insert' or 'delete' for
        text about to be inserted/replaced or delete, respectively; or None
        to mark a boundary between undo actions (i.e., a cursor move).
        """
        if not self.keep_undo or self._undo_last_type == cur_type:
            return
        self._close_undo_slot()
        if cur_type:
            # do not open a new slot for moves; moves just close the
            # currently opened slot
            self._set_undo_slot(cur_type)

    def undo(self):
        if self._undo_pos < 0:
            # nothing to undo
            return False
        self._close_undo_slot()
        pos = self._undo_pos
        if pos == len(self._undo_buffer) - 1:
            # we're starting undo, store the current state for redo
            self._set_undo_slot(None)
        self._undo_pos = pos - 1
        self._text, self._attr, cursor = self._undo_buffer[pos]
        self._apply_text_attr(cursor)
        return True

    def redo(self):
        if self._undo_pos >= len(self._undo_buffer) - 2:
            # nothing to redo; we never increment to point to the last slot
            return False
        self._undo_pos += 1
        self._text, self._attr, cursor = self._undo_buffer[self._undo_pos + 1]
        self._apply_text_attr(cursor)
        return True

    def _retab_line(self, y):
        self._text[y], self._attr[y] = retab(self._text[y], self._attr[y],
                                             self._tab_width, self._tab_offset)

    def _delete_text(self, x1, y1, x2, y2):
        ltext, lattr, _, _ = split_text_attr_line(self._text[y1], self._attr[y1], x1)
        _, _, rtext, rattr = split_text_attr_line(self._text[y2], self._attr[y2], x2)
        text, attr = combine_text_attr_line(ltext, lattr, rtext, rattr)
        self._text = list(itertools.chain(itertools.islice(self._text, 0, y1),
                                          (text,),
                                          itertools.islice(self._text, y2+1, None)))
        self._attr = list(itertools.chain(itertools.islice(self._attr, 0, y1),
                                          (attr,),
                                          itertools.islice(self._attr, y2+1, None)))
        self._retab_line(y1)
        return x1, y1

    def _delete_highlight(self):
        hstart, hend = self.highlight
        return self._delete_text(*hstart, *hend)

    def delete_highlight(self):
        if not self.highlight:
            return False
        self.add_undo('delete')
        x, y = self._delete_highlight()
        self._apply_text_attr((x, y))
        return True

    def insert_text(self, size, markup):
        """
        Insert text at the cursor position and update the cursor.
        This method is used by the keypress() method when inserting
        one or more characters into edit_text.
        """
        self.build_line_cache(size[0])
        self.add_undo('insert')
        if self.highlight:
            x, y = self._delete_highlight()
        else:
            x, y = self.get_text_cursor(size)
        ntext, nattr = decompose_tagmarkup(markup,
                                           tab_width=self._tab_width,
                                           tab_offset=self._tab_offset+x,
                                           keep_end_nl=True,
                                           default_attr=self._default_attr)
        # split the current text at the cursor location
        ltext, lattr, rtext, rattr = split_text_attr_line(self._text[y], self._attr[y], x)
        # combine the left part with the first line of the new text
        ntext[0], nattr[0] = combine_text_attr_line(ltext, lattr, ntext[0], nattr[0])
        # with the first line adjusted, calculate the new cursor position
        nx = len(ntext[-1])
        ny = y + len(ntext) - 1
        # combine the last line of the new text with the right part
        ntext[-1], nattr[-1] = combine_text_attr_line(ntext[-1], nattr[-1], rtext, rattr)
        # replace the original line with the new ones
        self._text = list(itertools.chain(itertools.islice(self._text, 0, y),
                                          ntext,
                                          itertools.islice(self._text, y+1, None)))
        self._attr = list(itertools.chain(itertools.islice(self._attr, 0, y),
                                          nattr,
                                          itertools.islice(self._attr, y+1, None)))
        # retab the last line; no need to retab the first line, it was
        # already tabbed correctly as we passed the right tab_offset to
        # decompose_tagmarkup
        self._retab_line(y + len(ntext) - 1)
        self._apply_text_attr((nx, ny))

    def delete_char(self, size, backspace=False):
        """
        Delete character at the cursor and update the cursor. If there's
        a highlight, the highlight is deleted instead.
        """
        self.build_line_cache(size[0])
        if self.delete_highlight():
            return True
        move = self._move_cursor_left if backspace else self._move_cursor_right
        x1, y1 = self.get_text_cursor(size)
        if not move(size):
            return False
        x2, y2 = self.get_text_cursor(size)
        if backspace:
            x1, y1, x2, y2 = x2, y2, x1, y1
        self.add_undo('delete')
        x, y = self._delete_text(x1, y1, x2, y2)
        self._apply_text_attr((x, y))
        return True

    def delete_line(self, size):
        """
        Delete line at the cursor and update the cursor. If there's
        a highlight, the highlight is deleted instead.
        """
        self.build_line_cache(size[0])
        if self.delete_highlight():
            return True
        ry = self._cursor_rendered[1]
        y, start, end, _, _ = self.lines[ry]
        x2, y2 = end, y
        if ry + 1 < len(self.lines):
            y2, x2, _, _, _ = self.lines[ry + 1]
            if y2 > y and start > 0:
                # do not consume the eol if we're only deleting the last part of
                # a wrapped line
                x2, y2 = end, y
        elif len(self._text) > 1:
            # we're deleting the last line
            y -= 1
            start = len(self._text[y])
        if start > 0 and self._text[y][start - 1] in (' ', '\t', '\x01'):
            # delete the preceding space if we wrapped there (otherwise the line
            # would still wrap at that space and would not appear as deleted)
            while self._text[y][start - 1] == '\x01':
                start -= 1
            start -= 1
        self.add_undo('delete')
        self._delete_text(start, y, x2, y2)
        self._apply_text_attr()
        return True

    def valid_char(self, ch):
        """
        Filter for text that may be entered into this widget by the user

        :param ch: character to be inserted
        :type ch: str

        This implementation returns True for all printable characters.
        """
        return len(ch)==1 and ord(ch) >= 32

    def keypress(self, size, key):
        key = super().keypress(size, key)
        if not key:
            return
        ckey = self._command_map[key]
        if self.valid_char(key):
            self.insert_text(size, key)
        elif key == 'enter' and self.multiline:
            self.insert_text(size, '\n')
        elif key == 'delete':
            if not self.delete_char(size):
                return key
        elif key == 'backspace':
            if not self.delete_char(size, backspace=True):
                return key
        elif ckey == DELETE_LINE:
            if not self.delete_line(size):
                return key
        elif ckey == PASTE:
            if not clipboard.text:
                return key
            if self.multiline:
                self.insert_text(size, clipboard.text)
            else:
                self.insert_text(size, clipboard.text.replace('\n', ' '))
        elif ckey == UNDO:
            if not self.undo():
                return key
        elif ckey == REDO:
            if not self.redo():
                return key
        else:
            return key


class TextContainerMixin(container.WidgetContainerMixin,
                         container.WidgetContainerListContentsMixin):
    signals = ['child-focus']

    contents = property(lambda self: self._contents)

    def _assert_text_widget(self, item):
        if not isinstance(item, TextBase):
            raise TypeError('Only Text widgets can be added to a text container, not {}'
                            .format(type(item)))

    def _normalize_widget_list(self, widget_list):
        """Returns a list of tuples (widget, enabled, sizing). Accepts
        various formats; see the TextColumns description for all options."""
        result = []
        if not widget_list:
            return result
        if (isinstance(widget_list, TextBase) or
            (isinstance(widget_list, tuple) and isinstance(widget_list[0], (int, str)))):
            # a single widget only
            widget_list = [widget_list]
        for item in widget_list:
            if isinstance(item, TextBase):
                # the item is directly a text widget
                result.append([item, True, None])
                continue
            elif isinstance(item[0], TextBase):
                # the item is (widget, enabled)
                result.append([item[0], item[1], None])
                continue
            elif isinstance(item[0], tuple):
                # the item is ((sizing, ..., widget), enabled)
                sizing = item[0]
                enabled = item[1]
            else:
                # the item is (sizing, ..., widget)
                sizing = item
                enabled = True
            if isinstance(sizing[0], int):
                self._assert_text_widget(sizing[1])
                result.append([sizing[1], enabled, ('fixed', sizing[0])])
            elif sizing[0] == 'pack':
                self._assert_text_widget(sizing[1])
                result.append([sizing[1], enabled, ('pack', 1)])
            elif sizing[0] == 'weight':
                self._assert_text_widget(sizing[2])
                result.append([sizing[2], enabled, ('weight', sizing[1])])
            else:
                raise TypeError('Unknown sizing specification "{}"'.format(sizing[0]))
        return result

    def _create_contents(self, widget_list):
        self._contents = monitored_list.MonitoredFocusList()
        self._contents.set_validate_contents_modified(self._validate_contents_modified)
        self._contents.extend(self._normalize_widget_list(widget_list))
        self._move_focus_end()
        self._contents.set_modified_callback(self._changed_all)
        self._contents.set_post_focus_changed_callback(self._changed_cursor)
        self._reset_selectable()

    def _validate_contents_modified(self, slc, new_items):
        for item, _, _ in new_items:
            self._assert_text_widget(item)
            signals.connect_signal(item, 'changed', self._changed_contents)
            signals.connect_signal(item, 'selectable-changed', self._changed_all,
                                   user_args=[True])
            signals.connect_signal(item, 'moved', self._changed_cursor)
            signals.connect_signal(item, 'selection', self._changed_cursor)

        start, stop, step = slc
        num_new_items = len(new_items)

        focus = self._contents.focus or 0
        direction = 1
        pos_new_items = num_new_items if focus > stop else 0
        while True:
            if focus in range(start, stop, step):
                focus += direction
            else:
                if focus == stop and pos_new_items < num_new_items:
                    widget, enabled, _ = new_items[pos_new_items]
                elif focus < len(self._contents):
                    widget, enabled, _ = self._contents[focus]
                else:
                    widget = None

                if widget and widget._selectable and enabled:
                    break

                if focus == stop:
                    pos_new_items += direction
                    if pos_new_items < 0:
                        pos_new_items = 0
                        focus -= 1
                    elif pos_new_items > num_new_items:
                        pos_new_items = num_new_items
                        focus += 1
                else:
                    focus += direction

            if focus > len(self._contents):
                # try the other direction
                focus = self._contents.focus or 0
                direction = -1
                pos_new_items = num_new_items
            elif focus < 0:
                # no selectable widget found; just return zero and let the
                # modified callback handle setting this container as
                # unselectable
                return 0

        # adjust for added and removed items
        focus += pos_new_items
        focus -= len(range(start, min(focus, stop), step))
        return focus

    def _reset_selectable(self):
        new_selectable = any(w._selectable and enabled for w, enabled, _ in self.contents)
        if new_selectable != self._selectable:
            self._selectable = new_selectable
            self._send_signal('selectable-changed')

    def _changed_all(self, adjust_focus=False):
        """
        Called when children widgets are replaced. All caches need to be
        rebuilt. If adjust_focus is False, the focus will be adjusted
        separately by the caller (usually via _validate_contents_modified).
        """
        self._reset_selectable()
        self._send_signal('changed')
        if adjust_focus:
            self.adjust_focus()
        self._invalidate()

    def _changed_cursor(self, *args):
        """
        Called when the cursor position changed or when a different child
        got focus. The arguments are to be ignored. Should force redraw of
        the child widgets but should not recalculate them.
        """
        if not self._selectable:
            # The monitored_list (self._contents) is unaware of whether the
            # widget is selectable or not. Just ignore any bogus focus
            # changes that happen while there's nothing selectable in the
            # container.
            return
        self._send_signal('moved', *self.get_pref_text_cursor(bounded=True))
        self._invalidate('moved')

    def _changed_contents(self):
        """
        Called when contents of some of the widgets got changed.
        Should force recalculation and redraw of all of the widgets but
        should not recalculate position of the widgets in the container.
        """
        self._send_signal('changed')
        self._invalidate('changed')

    def _get_focus_position(self):
        if not self._selectable:
            raise IndexError('Not selectable')
        return self.contents.focus

    def _set_focus_position(self, position):
        try:
            if position < 0 or position >= len(self.contents):
                raise IndexError
        except (TypeError, IndexError):
            raise IndexError('No child widget at position {}'.format(position))
        self.contents.focus = position
        signals.emit_signal(self, 'child-focus', position)

    def get_focus(self):
        if not self.contents:
            return None
        return self.contents[self.focus_position][0]

    def _move_focus_end(self, last=False):
        if last:
            iterator = range(len(self.contents) - 1, -1, -1)
        else:
            iterator = range(len(self.contents))
        for i in iterator:
            if self.contents[i][0]._selectable and self.contents[i][1]:
                self.focus_position = i
                return

    def adjust_focus(self):
        """
        Ensures that a visible widget is focused.
        """
        if not self._selectable:
            return
        focus_pos = self.focus_position
        widget, enabled, _ = self.contents[focus_pos]
        if widget._selectable and enabled:
            return
        for loop in (range(focus_pos + 1, len(self.contents)),
                     range(focus_pos - 1, -1, -1)):
            for i in loop:
                widget, enabled, _ = self.contents[i]
                if widget._selectable and enabled:
                    self.focus_position = i
                    return

    def set_enabled(self, index, enabled):
        """
        Enables (shows) or disables (hides) widget at the given
        index.
        """
        if self.contents[index][1] == enabled:
            return
        self.contents[index][1] = enabled
        self._changed_all(adjust_focus=True)

    def append(self, widget):
        """
        Appends a new text widget. Use instead of contents.append.
        """
        widget_list = self._normalize_widget_list(widget)
        if len(widget_list) != 1:
            raise ValueError('Expected a single widget, got {}'.format(len(widget_list)))
        self.contents.append(widget_list[0])

    def extend(self, widget_list):
        """
        Extends the content by a list of widgets. Use instead of
        contents.extend.

        Note that you can always use extend, even with a single widget. The
        append method is in fact superfluous.
        """
        self.contents.extend(self._normalize_widget_list(widget_list))

    def insert(self, index, widget, enabled=True):
        """
        Inserts a new text widget. Use instead of contents.insert.
        """
        widget_list = self._normalize_widget_list(widget)
        if len(widget_list) != 1:
            raise ValueError('Expected a single widget, got {}'.format(len(widget_list)))
        self.contents.insert(index, widget_list[0])

    def setitem(self, index, widget, copy=True):
        """
        Change a given text widget. Use instead of contents[index] = widget.
        If copy is True, the current enabled/disabled state and the current
        sizing is preserved.
        """
        widget_list = self._normalize_widget_list(widget)
        if len(widget_list) != 1:
            raise ValueError('Expected a single widget, got {}'.format(len(widget_list)))
        widget = widget_list[0]
        if copy:
            widget[1] = self.contents[index][1]
            widget[2] = self.contents[index][2]
        self.contents[index] = widget


class TextColumns(TextBase, TextContainerMixin):
    """
    Text widgets arranged horizontally. Similar to the Columns container but
    suitable for use in TextBox.
    """

    def __init__(self, widget_list, dividechars=0, sep=' '):
        """
        :param widget_list: a text widget, an iterable of text widgets, or
        an iterable of tuples (text widget, enabled)
        :param dividechars: number of blank characters between columns
        :param sep: the character to use as a blank character

        Each widget may also be a tuple of:

        (given_width, text widget)
            make this column given_width screen columns wide, where
            given_width is an int
        ('pack', text widget)
            call pack() on the widget to calculate the width of this column
        ('weight', weight, text widget)
            give this column a relative weight (number) to calculate its
            width from the screen columns remaining

        Widgets not in a tuple are the same as ('weight', 1, widget).
        """
        super().__init__(sep)
        self._widths = None
        self.dividechars = dividechars
        self._replace_cursor_ops()
        self._create_contents(widget_list)

    focus_position = property(lambda self: self._get_focus_position(),
                              lambda self, v: self._set_focus_position(v))
    focus = property(lambda self: self.get_focus())

    def _validate_contents_modified(self, slc, new_items):
        super()._validate_contents_modified(slc, new_items)
        for item, _, _ in new_items:
            item.set_in_column(True)

    def _invalidate(self, how=None):
        super()._invalidate()
        if how is None:
            # clear the cache only when the child widgets were replaced;
            # movement or content change in child widgets do not affect
            # widths
            self._widths = None

    def cache_widths(self, size):
        maxcol = size[0]
        if self._widths is not None and self._cache_maxcol == maxcol:
            return
        self._cache_maxcol = maxcol

        fixed = 0
        fixed_count = 0
        weights = 0
        weights_count = 0
        for w, enabled, sizing in self.contents:
            if not enabled:
                continue
            elif sizing is None:
                weights += 1
                weights_count += 1
            elif sizing[0] == 'weight':
                weights += sizing[1]
                weights_count += 1
            elif sizing[0] == 'fixed':
                fixed += sizing[1]
                fixed_count += 1
            elif sizing[0] == 'pack':
                fixed += w.pack(None)[0]
                fixed_count += 1
        total = fixed_count + weights_count

        available = maxcol - (total - 1) * self.dividechars
        # reserve at least one column for each weighted widget
        max_fixed = min(available - weights_count, fixed)
        weights_budget = available - max_fixed
        if weights == 0:
            weights_budget = 0

        self._widths = []
        fixed_used = 0
        weights_used = 0
        for w, enabled, sizing in self.contents:
            if not enabled:
                self._widths.append(0)
            elif sizing is None or sizing[0] == 'weight':
                if sizing is None:
                    width = 1
                else:
                    width = sizing[1]
                width = width * weights_budget // weights
                weights_used += width
                self._widths.append(width)
            elif sizing[0] == 'fixed' or sizing[0] == 'pack':
                if sizing[0] == 'fixed':
                    width = sizing[1]
                else:
                    width = w.pack(None)[0]
                if max_fixed < fixed:
                    width = width * max_fixed // fixed
                fixed_used += width
                self._widths.append(width)

        # distribute the remainders uniformly among widgets
        while fixed_used < max_fixed or weights_used < weights_budget:
            for i, (w, enabled, sizing) in enumerate(self.contents):
                if not enabled:
                    continue
                elif sizing is None or sizing[0] == 'weight':
                    if weights_used < weights_budget:
                        self._widths[i] += 1
                        weights_used += 1
                elif sizing[0] == 'fixed' or sizing[0] == 'pack':
                    if fixed_used < max_fixed:
                        self._widths[i] += 1
                        fixed_used += 1

    def __getattr__(self, name):
        """
        Delegates operations to the focused widget.
        """
        if name in ('search', 'get_pref_text_cursor', 'set_pref_text_cursor'):
            return getattr(self.focus, name)
        return getattr(super(), name)

    def _replace_cursor_ops(self):
        # TODO: change to a metaclass
        def new_method(op):
            def func(self, size, *args, **kwargs):
                self.cache_widths(size)
                width = self._widths[self.focus_position]
                size = (width,) if len(size) == 1 else (width, size[1])
                orig_func = getattr(self.focus, op)
                return orig_func(size, *args, **kwargs)
            # convert function to a method
            return func.__get__(self, self.__class__)

        for op in ('get_pref_rendered_cursor', 'set_pref_rendered_cursor',
                   'get_text_cursor', 'get_viewport_rendered_cursor',
                   'get_pref_col', 'move_cursor_to_coords',
                   'get_cursor_coords'):
            setattr(self, op, new_method(op))

    def _set_focus_position(self, position):
        super()._set_focus_position(position)
        # We have to emit the 'changed' signal because the number of active
        # rows can be different in different children.
        signals.emit_signal(self, 'changed')

    def rows(self, size, focus=False):
        self.cache_widths(size)
        return max(w.rows((self._widths[i],), focus)
                   for i, (w, enabled, _) in enumerate(self.contents) if enabled)

    def active_rows(self, size):
        # If the currently focused column is shorter than the rest of the
        # columns, there will be inactive area at the bottom.
        self.cache_widths(size)
        return self.focus.active_rows((self._widths[self.focus_position],))

    def text_lines(self):
        return max(w.text_lines() for w, enabled, _ in self.contents if enabled)

    def pack(self, size=None, focus=False):
        cnt = sum(int(enabled) for _, enabled, _ in self.contents)
        width = (cnt - 1) * self.dividechars
        height = 0
        focus_pos = self.focus_position
        for i, (w, enabled, _) in enumerate(self.contents):
            if not enabled:
                continue
            x, y = w.pack(size, focus and (i == focus_pos))
            width += x
            height = max(height, y)
        return width, height

    def subrender(self, size, origin_y, focus=False):
        _, rows = size
        self.cache_widths(size)
        focus_pos = self.focus_position
        canvases = []
        cnt = 0
        for i, (w, enabled, _) in enumerate(self.contents):
            if not enabled:
                continue
            if cnt > 0 and self.dividechars:
                c = canvas.SolidCanvas(self._padding_char, self.dividechars, rows)
                if self._default_attr:
                    c = canvas.CompositeCanvas(c)
                    c.fill_attr_apply({ None: self._default_attr })
                canvases.append((c, False, self.dividechars))
            has_focus = i == focus_pos
            c = w.subrender((self._widths[i], rows), origin_y, focus and has_focus)
            canvases.append((c, has_focus, self._widths[i]))
            cnt += 1
        return canvas.CanvasJoin(canvases)

    def render(self, size, focus=False):
        return super().render.original_fn(self, size, focus)

    def _move_focus(self, delta):
        pos = old_pos = self.focus_position
        width = self._widths[pos]
        x = 'right' if delta < 0 else 'left'
        y = self.contents[pos][0].get_pref_rendered_cursor((width,))[1]
        max_pos = len(self.contents) - 1
        while True:
            pos += delta
            if pos < 0:
                if self._in_column:
                    return False
                pos = max_pos
            elif pos > max_pos:
                if self._in_column:
                    return False
                pos = 0
            # We'll always stop. Worst case, we wrap to the old_pos widget,
            # which will pass the condition below. We will then refocus it,
            # positioning the cursor as expected.
            widget, enabled, _ = self.contents[pos]
            if enabled and widget._selectable:
                break
        width = self._widths[pos]
        widget.set_pref_rendered_cursor((width,), x, y)
        widget.unpref_cursor((width,))
        self.focus_position = pos
        return True

    def keypress(self, size, key):
        self.cache_widths(size)
        width = self._widths[self.focus_position]
        size = (width,) if len(size) == 1 else (width, size[1])
        key = self.focus.keypress(size, key)
        if not key:
            return
        ckey = self._command_map[key]
        if ckey == CURSOR_LEFT:
            if not self._move_focus(-1):
                return key
        elif ckey == CURSOR_RIGHT:
            if not self._move_focus(1):
                return key
        else:
            return key


class TextPile(TextBase, TextContainerMixin):
    """
    A pile of Text widgets (and its subclasses) stacked vertically. See the
    `Pile` widget documentation for description of the methods.
    """

    # Should be the up/down and page up/down keys left unhandled (and thus
    # passed to the parent widget) if the resulting position is not
    # contained within this widget?
    _pass_move = True

    def __init__(self, widget_list):
        """
        widget_list is a text widget, an iterable of text widgets or an
        iterable of tuples (text widget, enabled).
        """
        super().__init__()
        # cache of rendered line counts of individual widgets
        self._rendered_line_offsets = None
        # cache of text line counts of individual widgets
        self._text_line_offsets = None
        self._create_contents(widget_list)

    focus_position = property(lambda self: self._get_focus_position(),
                              lambda self, v: self._set_focus_position(v))
    focus = property(lambda self: self.get_focus())

    def _invalidate(self, how=None):
        super()._invalidate()
        if how != 'moved':
            # if children widgets got replaced or child widget content got
            # modified, we need to discard the line count caches
            self._rendered_line_offsets = None
            self._text_line_offsets = None

    def _get_cursor_y(self, maxcol):
        """
        Returns the absolute y value of the cursor. Expects a valid offset
        cache.
        """
        focus_pos = self.focus_position
        return self._rendered_line_offsets[focus_pos] + \
               self.contents[focus_pos][0].get_pref_rendered_cursor((maxcol,))[1]

    def cache_rendered_line_offsets(self, maxcol):
        if self._rendered_line_offsets is not None and self._cache_maxcol == maxcol:
            return False

        self._cache_maxcol = maxcol
        self._rendered_line_offsets = []
        self._active_rows = []
        self._pile_active_rows = [None, None]
        cnt = 0
        for w, enabled, _ in self.contents:
            ar = w.active_rows((maxcol,))
            self._active_rows.append(ar)
            self._rendered_line_offsets.append(cnt)
            if enabled:
                if w._selectable and ar[0] is not None:
                    if self._pile_active_rows[0] is None:
                        self._pile_active_rows[0] = cnt + ar[0]
                    self._pile_active_rows[1] = cnt + ar[1]
                cnt += w.rows((maxcol,))
        self._rendered_line_offsets.append(cnt)
        return True

    def cache_text_line_offsets(self):
        if self._text_line_offsets is not None:
            return False

        self._text_line_offsets = []
        cnt = 0
        for w, enabled, _ in self.contents:
            self._text_line_offsets.append(cnt)
            if enabled:
                cnt += w.text_lines()
        self._text_line_offsets.append(cnt)
        return True

    def rows(self, size, focus=False):
        self.cache_rendered_line_offsets(size[0])
        return self._rendered_line_offsets[-1]

    def active_rows(self, size):
        self.cache_rendered_line_offsets(size[0])
        return self._pile_active_rows

    def text_lines(self):
        self.cache_text_line_offsets()
        return self._text_line_offsets[-1]

    def pack(self, size=None, focus=False):
        width = height = 0
        focus_pos = self.focus_position if self._selectable else -1
        for i, (w, enabled, _) in enumerate(self.contents):
            if not enabled:
                continue
            x, y = w.pack(size, focus and (i == focus_pos))
            width = max(width, x)
            height += y
        return width, height

    def subrender(self, size, origin_y, focus=False):
        cols, rows = size
        self.cache_rendered_line_offsets(cols)
        focus_pos = self.focus_position if self._selectable else -1
        y = origin_y
        y_end = origin_y + rows
        canvases = []
        for i, (w, enabled, _) in enumerate(self.contents):
            if not enabled:
                continue
            if self._rendered_line_offsets[i + 1] <= y:
                continue
            has_focus = i == focus_pos
            new_y = min(y_end, self._rendered_line_offsets[i + 1])
            c = w.subrender((cols, new_y-y), y-self._rendered_line_offsets[i],
                            focus and has_focus)
            canvases.append((c, has_focus))
            y = new_y
            if y == y_end:
                break
        if y < y_end:
            # fill the rest of the space
            canvases.append((canvas.SolidCanvas(' ', cols, y_end-y), False))
        if not canvases:
            # Nothing rendered; this happens when all widgets are disabled
            # and rows == 0. We need to return a canvas with the proper width.
            return canvas.SolidCanvas(' ', cols, 0)
        return canvas.CanvasCombine(canvases)

    def render(self, size, focus=False):
        return super().render.original_fn(self, size, focus)

    def get_pref_rendered_cursor(self, size):
        self.cache_rendered_line_offsets(size[0])
        focus_pos = self.focus_position
        x, y = self.contents[focus_pos][0].get_pref_rendered_cursor(size)
        return (x, y + self._rendered_line_offsets[focus_pos])

    def set_pref_rendered_cursor(self, size, x, y):
        cols = size[0]
        self.cache_rendered_line_offsets(cols)
        delta_y = y - self._get_cursor_y(cols)
        self._move_focus(cols, x, delta_y)

    def get_pref_text_cursor(self, bounded=False):
        self.cache_text_line_offsets()
        focus_pos = self.focus_position
        x, y = self.contents[focus_pos][0].get_pref_text_cursor(bounded)
        return (x, y + self._text_line_offsets[focus_pos])

    def set_pref_text_cursor(self, x, y):
        self.cache_text_line_offsets()
        for i, ofs in enumerate(self._text_line_offsets):
            if y < ofs:
                widget, enabled, _ = self.contents[i - 1]
                if not enabled:
                    continue
                if not widget._selectable:
                    return False
                self.focus_position = i - 1
                widget.set_pref_text_cursor(x, y - self._text_line_offsets[i - 1])
                return True
        return False

    def _move_focus(self, cols, pref_x, delta_y):
        y = self._get_cursor_y(cols)
        target = y + delta_y
        pos = self.focus_position
        last_selectable = None
        rel_y = y - self._rendered_line_offsets[pos]
        if target < y and rel_y == self._active_rows[pos][0]:
            # skip the first widget if we're starting at its first line
            pos -= 1
        elif target > y and rel_y == self._active_rows[pos][1] - 1:
            # skip the first widget if we're starting at its last line
            pos += 1
        while pos >= 0 and pos < len(self.contents):
            old_pos = pos
            ar = self._active_rows[pos]
            if target < y:
                # moving backwards
                if ar[1] is not None:
                    # Adjust target in case it is in inactive area at the end of
                    # this widget (or in inactive area at the beginning of the
                    # previous widget). If ar is (None, None), then the
                    # widget is disabled; we assert that later.
                    target = min(target, self._rendered_line_offsets[pos] + ar[1] - 1)
                y = max(self._rendered_line_offsets[pos], target)
                pos -= 1
            else:
                # moving forward
                if ar[0] is not None:
                    # adjust target
                    target = max(target, self._rendered_line_offsets[pos] + ar[0])
                y = min(self._rendered_line_offsets[pos + 1] - 1, target)
                pos += 1
            widget, enabled, _ = self.contents[old_pos]
            if not enabled:
                continue
            if widget._selectable:
                assert ar[0] is not None and ar[1] is not None
                last_selectable = (old_pos, y - self._rendered_line_offsets[old_pos])
            if y == target:
                if last_selectable:
                    break
                # we reached the target but did not find anything selectable,
                # continue moving in the same direction
                if delta_y < 0:
                    target -= 1
                else:
                    target += 1
        else:
            if self._pass_move:
                return False

        if not last_selectable:
            return False
        pos, y = last_selectable
        self.contents[pos][0].set_pref_rendered_cursor((cols,), pref_x, y)
        self.focus_position = pos
        return True

    def keypress(self, size, key):
        if not self.contents:
            return key
        cols, rows = size
        self.cache_rendered_line_offsets(cols)

        # override the widgets' behavior for ctrl+home/end
        ckey = self._command_map[key]
        if ckey == CURSOR_TOP:
            self._move_focus_end()
            focus_pos = self.focus_position
            y = self._active_rows[focus_pos][0]
            self.contents[focus_pos][0].set_pref_rendered_cursor((cols,), 'left', y)
            return None
        elif ckey == CURSOR_BOTTOM:
            self._move_focus_end(last=True)
            focus_pos = self.focus_position
            y = self._active_rows[focus_pos][1] - 1
            self.contents[focus_pos][0].set_pref_rendered_cursor((cols,), 'right', y)
            return None

        focus_pos = self.focus_position
        key = self.contents[focus_pos][0].keypress(size, key)
        if key is None:
            return None
        ckey = self._command_map[key]
        if ckey == CURSOR_LEFT:
            if not self._move_focus(cols, 'right', -1):
                return key
        elif ckey == CURSOR_RIGHT:
            if not self._move_focus(cols, 'left', 1):
                return key
        elif ckey in (CURSOR_UP, CURSOR_DOWN, CURSOR_PAGE_UP, CURSOR_PAGE_DOWN):
            pref_x = self.contents[focus_pos][0].get_pref_rendered_cursor((cols,))[0]
            if ckey == CURSOR_UP:
                delta_y = -1
            elif ckey == CURSOR_DOWN:
                delta_y = 1
            elif ckey == CURSOR_PAGE_UP:
                delta_y = -rows
            elif ckey == CURSOR_PAGE_DOWN:
                delta_y = rows
            if not self._move_focus(cols, pref_x, delta_y):
                return key
        else:
            return key

    def search(self, regex, from_cursor=True, backwards=False):
        """
        Searches for the given regex from the current cursor position and
        jumps to it. Returns False if it was not found.
        """
        self.cache_text_line_offsets()
        pos = self.focus_position if from_cursor else 0
        while pos >= 0 and pos < len(self.contents):
            widget, enabled, _ = self.contents[pos]
            if widget._selectable and enabled:
                found = widget.search(regex, from_cursor, backwards)
                if found:
                    return (found[0], found[1] + self._text_line_offsets[pos])
            from_cursor = False
            pos += 1 if not backwards else -1
        return None


class TextBox(TextPile):
    """
    TextBox turns a stack of flow text widgets into a box widget. It
    maintains a viewport into this stack, which means that cursor movement
    acts naturally, i.e. the viewport is maintained at the same place while
    the cursor stays within it and is moved only when the cursor is moved
    out of it. It also instructs the text widgets to render only the part of
    them that is visible, which gains a major speedup.
    """
    _sizing = frozenset([widget.BOX])
    _pass_move = False

    def __init__(self, widget_list):
        """
        widget_list is a text widget or an iterable of text widgets.
        """
        super().__init__(widget_list)
        # at what rendered line we want to have the cursor after a resize
        self._screen_cursor_y = 0
        # where the viewport starts (None means "recalculate")
        self._start_y = None

    def _invalidate(self, how=None):
        super()._invalidate(how)
        if how is None:
            # Keep the viewport position on both movement and content
            # change.
            self._start_y = None

    def cache_rendered_line_offsets(self, maxcol):
        if super().cache_rendered_line_offsets(maxcol):
            if self._start_y is None:
                # Recalculate self._start_y to have the cursor on the same y position
                # as it was before. We rely on this code not being executed solely
                # for cursor movement.
                self._start_y = max(0, self._get_cursor_y(maxcol) - self._screen_cursor_y)

    def update_viewport(self, size):
        """
        Adjust self._start_y to have the cursor on screen and update
        self._screen_cursor_y.
        """
        cols, rows = size
        self.cache_rendered_line_offsets(cols)
        y = self._get_cursor_y(cols)
        if y < self._start_y:
            self._start_y = y
        elif y >= self._start_y + rows:
            self._start_y = y - rows + 1
        bottom = max(0, self._rendered_line_offsets[-1] - rows)
        if self._start_y > bottom:
            self._start_y = bottom
        self._screen_cursor_y = y - self._start_y

    def render(self, size, focus=False):
        self.update_viewport(size)
        return self.subrender(size, self._start_y, focus)

    def search(self, regex, from_cursor=True, backwards=False):
        """
        Searches for the given regex and jumps to it. Returns False if it
        was not found.
        """
        found = super().search(regex, from_cursor, backwards)
        if not found:
            return False
        return self.set_pref_text_cursor(*found)
