# Urwid utility functions
#    Copyright (C) 2004-2011  Ian Ward
#
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Urwid web site: http://excess.org/urwid/

from urwid import escape

import codecs


def calc_trim_text( text, start_offs, end_offs, start_col, end_col ):
    """
    Calculate the result of trimming text.
    start_offs -- offset into text to treat as screen column 0
    end_offs -- offset into text to treat as the end of the line
    start_col -- screen column to trim at the left
    end_col -- screen column to trim at the right

    Returns (start, end, pad_left, pad_right), where:
    start -- resulting start offset
    end -- resulting end offset
    pad_left -- 0 for no pad or 1 for one space to be added
    pad_right -- 0 for no pad or 1 for one space to be added
    """
    spos = start_offs
    pad_left = pad_right = 0
    if start_col > 0:
        if start_col < end_offs:
            spos += start_col
        else:
            pad_left = 1
            spos += end_offs - start_offs
    run = end_col - start_col - pad_left
    if run < end_offs:
        pos = spos + run
    else:
        pos = end_offs
        pad_right = 1
    return ( spos, pos, pad_left, pad_right )


def trim_text_attr( text, attr, start_col, end_col ):
    """
    Return ( trimmed text, trimmed attr ).
    """
    spos, epos, pad_left, pad_right = calc_trim_text(
        text, 0, len(text), start_col, end_col )
    attrtr = rle_subseg( attr, spos, epos )
    if pad_left:
        al = rle_get_at( attr, spos-1 )
        rle_prepend_modify( attrtr, (al, 1) )
    if pad_right:
        al = rle_get_at( attr, epos )
        rle_append_modify( attrtr, (al, 1) )

    return (''.rjust(pad_left) + text[spos:epos] +
        ''.rjust(pad_right), attrtr)


def rle_get_at( rle, pos ):
    """
    Return the attribute at offset pos.
    """
    x = 0
    if pos < 0:
        return None
    for a, run in rle:
        if x+run > pos:
            return a
        x += run
    return None


def rle_subseg( rle, start, end ):
    """Return a sub segment of an rle list."""
    l = []
    x = 0
    for a, run in rle:
        if start:
            if start >= run:
                start -= run
                x += run
                continue
            x += start
            run -= start
            start = 0
        if x >= end:
            break
        if x+run > end:
            run = end-x
        x += run
        l.append( (a, run) )
    return l


def rle_len( rle ):
    """
    Return the number of characters covered by a run length
    encoded attribute list.
    """

    run = 0
    for v in rle:
        assert type(v) == tuple, repr(rle)
        a, r = v
        run += r
    return run

def rle_prepend_modify(rle, a_r):
    """
    Append (a, r) (unpacked from *a_r*) to BEGINNING of rle.
    Merge with first run when possible

    MODIFIES rle parameter contents. Returns None.
    """
    a, r = a_r
    if not rle:
        rle[:] = [(a, r)]
    else:
        al, run = rle[0]
        if a == al:
            rle[0] = (a,run+r)
        else:
            rle[0:0] = [(a, r)]


def rle_append_modify(rle, a_r):
    """
    Append (a, r) (unpacked from *a_r*) to the rle list rle.
    Merge with last run when possible.

    MODIFIES rle parameter contents. Returns None.
    """
    a, r = a_r
    if not rle or rle[-1][0] != a:
        rle.append( (a,r) )
        return
    la,lr = rle[-1]
    rle[-1] = (a, lr+r)

def rle_join_modify( rle, rle2 ):
    """
    Append attribute list rle2 to rle.
    Merge last run of rle with first run of rle2 when possible.

    MODIFIES attr parameter contents. Returns None.
    """
    if not rle2:
        return
    rle_append_modify(rle, rle2[0])
    rle += rle2[1:]


class TagMarkupException(Exception): pass

def decompose_tagmarkup(tm):
    """Return (text string, attribute list) for tagmarkup passed."""

    tl, al = _tagmarkup_recurse(tm, None)
    text = ''.join(tl)

    if al and al[-1][0] is None:
        del al[-1]

    return text, al

def _tagmarkup_recurse( tm, attr ):
    """Return (text list, attribute list) for tagmarkup passed.

    tm -- tagmarkup
    attr -- current attribute or None"""

    if isinstance(tm, list):
        # for lists recurse to process each subelement
        rtl = []
        ral = []
        for element in tm:
            tl, al = _tagmarkup_recurse( element, attr )
            if ral:
                # merge attributes when possible
                last_attr, last_run = ral[-1]
                top_attr, top_run = al[0]
                if last_attr == top_attr:
                    ral[-1] = (top_attr, last_run + top_run)
                    del al[-1]
            rtl += tl
            ral += al
        return rtl, ral

    if isinstance(tm, tuple):
        # tuples mark a new attribute boundary
        if len(tm) != 2:
            raise TagMarkupException("Tuples must be in the form (attribute, tagmarkup): %r" % (tm,))

        attr, element = tm
        return _tagmarkup_recurse( element, attr )

    if not isinstance(tm, str):
        raise TagMarkupException("Invalid markup element: %r" % tm)

    # text
    return [tm], [(attr, len(tm))]



def is_mouse_event( ev ):
    return type(ev) == tuple and len(ev)==4 and ev[0].find("mouse")>=0

def is_mouse_press( ev ):
    return ev.find("press")>=0



class MetaSuper(type):
    """adding .__super"""
    def __init__(cls, name, bases, d):
        super(MetaSuper, cls).__init__(name, bases, d)
        if hasattr(cls, "_%s__super" % name):
            raise AttributeError("Class has same name as one of its super classes")
        setattr(cls, "_%s__super" % name, super(cls))



def int_scale(val, val_range, out_range):
    """
    Scale val in the range [0, val_range-1] to an integer in the range
    [0, out_range-1].  This implementation uses the "round-half-up" rounding
    method.

    >>> "%x" % int_scale(0x7, 0x10, 0x10000)
    '7777'
    >>> "%x" % int_scale(0x5f, 0x100, 0x10)
    '6'
    >>> int_scale(2, 6, 101)
    40
    >>> int_scale(1, 3, 4)
    2
    """
    num = int(val * (out_range-1) * 2 + (val_range-1))
    dem = ((val_range-1) * 2)
    # if num % dem == 0 then we are exactly half-way and have rounded up.
    return num // dem


class StoppingContext(object):
    """Context manager that calls ``stop`` on a given object on exit.  Used to
    make the ``start`` method on `MainLoop` and `BaseScreen` optionally act as
    context managers.
    """
    def __init__(self, wrapped):
        self._wrapped = wrapped

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self._wrapped.stop()
